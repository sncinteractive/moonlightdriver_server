var Async			= require('async');
var User			= DB.model('User');
var MyCarInfo		= DB.model('MyCarInfo');
var Favorite		= DB.model('Favorite');
var CreditCard		= DB.model('CreditCard');
var UserPointHist	= DB.model('UserPointHist');
var UserAdditional	= DB.model('UserAdditional');
var _LogCreditCard	= DB.model('_LogCreditCard');
var ObjectId		= DB.Types.ObjectId;

function initialize (server) {
	server.post('/settings/get/my_car_info', getMyCarInfo);
	server.post('/settings/save/my_car_info', saveMyCarInfo);
	server.post('/settings/save/favorite', saveFavorite);
	server.post('/settings/get/favorite', getFavorite);
	server.post('/settings/delete/favorite', deleteFavorite);
	
	server.post('/settings/add/card', addCard);
	server.post('/settings/list/card', listCard);
	server.post('/settings/delete/card', deleteCard);
	
	server.post('/settings/list/point', listPoint);
	server.post('/settings/charge/point', chargePoint);
}

function getMyCarInfo(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function(next) {
 			MyCarInfo.findOne({userId: req.params.userId}).exec(function(err, myCarInfo) {
  				if(err) {
  					next(err);
  				} else {
  					var retMyCarInfo = new MyCarInfo();
  					if(myCarInfo) {
  						retMyCarInfo = myCarInfo.getData();
  					}
  		 			
  		 			res.send({ result: error_code.SUCCESS, retMyCarInfo: retMyCarInfo});
  					next();
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function saveMyCarInfo(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function(next) {
 			MyCarInfo.findOne({userId: req.params.userId}).exec(function(err, myCarInfo) {
  				if(err) {
  					next(err);
  				} else {
  					if(!myCarInfo) {
  						myCarInfo = new MyCarInfo();
  					}
  					
  					myCarInfo.userId = req.params.userId;
  					if(req.params.carType) {
  						myCarInfo.carType = req.params.carType;
					}
					
					if(req.params.carMadeType) {
						myCarInfo.carMadeType = req.params.carMadeType;
					}
					
					if(req.params.transmissionType) {
						myCarInfo.transmissionType = req.params.transmissionType;
					}
					
					myCarInfo.save(function(err) {
						if(err) {
							next(err);
						} else {
							res.send({ result: error_code.SUCCESS, retMyCarInfo: myCarInfo});
		  					next();
						}
					});  		 			
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function saveFavorite(req, res) {
	if(svr_utils.checkParams(['userId', 'favoriteName', 'locationName', 'locationAddress', 'lat', 'lng'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function(next) {
 			var favorite = new Favorite({
				userId : req.params.userId,
				favoriteName : req.params.favoriteName,
	 			locationName : req.params.locationName,
	 			locationAddress : req.params.locationAddress,
	 			lat : req.params.lat,
	 			lng : req.params.lng,
	 			createTime: new Date().getTime()
			});
 			
 			favorite.save(function(_err) {
 				if(_err) {
 					next(_err);
 				} else {
 					next(null, favorite);
 				}
 			});
 		},
 		function(favorite, next) {
 			res.send({ result: error_code.SUCCESS, favorite: favorite });
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getFavorite(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function(next) {
 			Favorite.find({userId: req.params.userId}).exec(function(err, favoriteList) {
  				if(err) {
  					next(err);
  				} else {
  					var retList = [];
  					
  					if(favoriteList && favoriteList.length > 0) {  						
  						for(var i = 0; i < favoriteList.length; i++) {
  							retList.push(favoriteList[i].getData());
  						}
  					}
  					
  					res.send({ result: error_code.SUCCESS, favoriteList: retList});
  					next();
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function deleteFavorite(req, res) {
	if(svr_utils.checkParams(['userId', 'removedList'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	req.params.removedList = unescape(req.params.removedList).split(",");
	
	Async.waterfall([
 		function(next) {
 			var removeListIds = [];
 			
 			for(var i = 0; i < req.params.removedList.length; i++) {
 				if(req.params.removedList[i]) {
 					console.log(req.params.removedList[i]);
 					removeListIds.push(new ObjectId(req.params.removedList[i]));
 				}
 			}
 			
 			if(removeListIds.length > 0) {
 				Favorite.remove({"_id" : {'$in' : removeListIds}}, function(err, data) {
 					if(err) {
 						next(err);
 					} else {
 						next();
 					}
 				});
 			} else {
 				next();
 			}
 		}, function(next) {
 			res.send({ result: error_code.SUCCESS });
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function addCard(req, res) {
	if(svr_utils.checkParams(['userId', 'cardNumber', 'userName', 'userEmail', 'companyCardYN'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			var creditCard = new CreditCard({
				userId: req.params.userId,
				cardNumber: req.params.cardNumber,
				userName: req.params.userName,
				userEmail: req.params.userEmail,
				companyCardYN: req.params.companyCardYN,
				createTime: new Date().getTime()
			});
			
			creditCard.save(function(_err) {
				next(_err, creditCard);
			})
		}, function(creditCard, next) {
 			res.send({ result: error_code.SUCCESS });
 			
 			creditCard = creditCard.getData();
 			
 			var log_data = new _LogCreditCard({
 				creditCardId: creditCard.creditCardId,
 				userId: creditCard.userId,
				cardNumber: creditCard.cardNumber,
				userName: creditCard.userName,
				userEmail: creditCard.userEmail,
				companyCardYN: creditCard.companyCardYN,
				createTime: creditCard.createTime,
				logType: "register",
				lodDate: new Date()
 			});
 			
 			log_data.save();
 			
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function listCard(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			CreditCard.find({userId: req.params.userId}).exec(next);
		}, function(creditCardList, next) {
			var retCreditCardList = [];
			if(creditCardList) {
				for(var i = 0; i < creditCardList.length; i++) {
					retCreditCardList.push(creditCardList[i].getData());
				}
			}
			
			res.send({ result: error_code.SUCCESS, creditCardList :retCreditCardList });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function deleteCard(req, res) {
	if(svr_utils.checkParams(['userId', 'removedList'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	req.params.removedList = unescape(req.params.removedList).split(",");
	
	var removeListIds = [];
		
	for(var i = 0; i < req.params.removedList.length; i++) {
		if(req.params.removedList[i]) {
			removeListIds.push(new ObjectId(req.params.removedList[i]));
		}
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
 			if(removeListIds.length > 0) {
 				CreditCard.find({"_id" : {'$in' : removeListIds}, userId: req.params.userId}, function(err, data) {
 					next(err, data);
 				});
 			} else {
 				res.send({ result: error_code.SUCCESS });
 				return;
 			}
 		}, function(creditCardList, next) {
 			if(creditCardList && creditCardList.length > 0) {
 				CreditCard.remove({"_id" : {'$in' : removeListIds}}, function(err, data) {
 					next(err, creditCardList);
 				});
 			} else {
 				res.send({ result: error_code.NO_SUCH_CARD });
 				return;
 			}
 		}, function(creditCardList, next) {
 			for(var i = 0; i < creditCardList.length; i ++) {
 				var creditCard = creditCardList[i].getData();
 				var log_data = new _LogCreditCard({
 	 				creditCardId: creditCard.creditCardId,
 	 				userId: creditCard.userId,
 					cardNumber: creditCard.cardNumber,
 					userName: creditCard.userName,
 					userEmail: creditCard.userEmail,
 					companyCardYN: creditCard.companyCardYN,
 					createTime: creditCard.createTime,
 					logType: "delete",
 					lodDate: new Date()
 	 			});
 	 			
 	 			log_data.save();
 			}
 			
 			res.send({ result: error_code.SUCCESS });
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function listPoint(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			UserPointHist.find({userId: req.params.userId}).exec(next);
		}, function(pointHistList, next) {
			var retPointHistList = [];
			if(pointHistList) {
				for(var i = 0; i < pointHistList.length; i++) {
					retPointHistList.push(pointHistList[i].getData());
				}
			}
			
			res.send({ result: error_code.SUCCESS, pointHistList :retPointHistList });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function chargePoint(req, res) {
	if(svr_utils.checkParams(['userId', 'point'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var chargePoint = req.params.point ? parseInt(req.params.point, 10) : 0;
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			var userInfo = userInfo.getData();
			
			UserAdditional.findOne({"userId" : userInfo.userId}).exec(function(_err, _userAdditional) {
				if(_err) {
					next(_err);
				} else {
					if(!_userAdditional) {
						var _userAdditional = new UserAdditional({
							userId : userInfo.userId,
							pushYN : "Y",
							point : 0,
							createdDate : new Date(),
							lastUpdateDate : new Date()
						});
						
						_userAdditional.save(function(err) {
							next(err, userInfo, _userAdditional);
						});
					} else {
						next(null, userInfo, _userAdditional);
					}
				}
			});
		}, function(_userInfo, _userAdditional, next) {
			var userHistPoint = UserPointHist({
				userId : req.params.userId,
			    point : chargePoint,
			    beforePoint : _userAdditional.point,
			    afterPoint : _userAdditional.point + chargePoint,
			    pointMessage : "포인트 충전",
			    pointType : "charge",
			    from : "",
			    to : "",
			    price : 0,
			    driverInfo : null,
			    createdDate : new Date().getTime()
			});
			
			userHistPoint.save(function(err) {
	    		next(err, _userInfo, _userAdditional);
			});
 		}, function(_userInfo, _userAdditional, next) {
 			_userAdditional.point = _userAdditional.point + chargePoint;
 			
 			_userAdditional.save(function(err) {
	    		next(err, _userAdditional);
			});
 		}, function(_userAdditional, next) {
 			res.send({ result: error_code.SUCCESS, userAdditional :_userAdditional.getData() });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

exports.initialize = initialize;
exports.getMyCarInfo = getMyCarInfo;
exports.saveMyCarInfo = saveMyCarInfo;
exports.saveFavorite = saveFavorite;
exports.getFavorite = getFavorite;
exports.deleteFavorite = deleteFavorite;
exports.addCard = addCard;
exports.listCard = listCard;
exports.deleteCard = deleteCard;
exports.listPoint = listPoint;
exports.chargePoint = chargePoint;

