var Async				= require('async');
var User      			= DB.model('User');
var CardariDriver  		= DB.model('CardariDriver');
var Call				= DB.model('Call');
var DriverEvaluate		= DB.model('DriverEvaluate');
var DriverUnfairness 	= DB.model('DriverUnfairness');
var _LogCall			= DB.model('_LogCall');
var _LogDriverBalance	= DB.model('_LogDriverBalance');
var ObjectId			= DB.Types.ObjectId;
var PushMessage			= require(rootPath + '/lib/PushMessage');

function initialize (server) {
	server.post('/calldriver/save/call', saveCall);
	server.post('/calldriver/cancel/call', cancelCall);
	server.post('/calldriver/update/relief_message_state', updateUseReliefMessage);
	server.post('/calldriver/history/call', callHistory);
	server.post('/calldriver/evaluate/driver', evaluateDriver);
	server.post('/calldriver/unfairness/driver', unfairnessDriver);
	server.post('/calldriver/calculate/price', calculatePrice);
	server.post('/calldriver/priceoffer', priceOffer);
	server.post('/calldriver/detail', callDetail);
	server.post('/calldriver/update/payment', callUpdatePayment);
}

function saveCall (req, res) {
	var essentialParams = ['uniqueId', 'userId', 'user_address', 'user_city_do', 'user_gu_gun', 'user_lat', 'user_lng', 
							'start_lat', 'start_lng', 'start_address', 'end_lat', 'end_lng', 'end_address', 'money', 'callType', 'payment_option', 'status'];
	if (svr_utils.checkParams(essentialParams, req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			var through = {
				address: '',
				location: {
					type: "Point",
					coordinates: []
				}
			};
			
			var through1 = {
				address: '',
				location: {
					type: "Point",
					coordinates: []
				}
			};
			
			var through2 = {
				address: '',
				location: {
					type: "Point",
					coordinates: []
				}
			};
			
			if(req.params.through_address) {
				through.address = req.params.through_address,
				through.location.coordinates = [parseFloat(req.params.through_lng), parseFloat(req.params.through_lat)];
			}
			
			if(req.params.through1_address) {
				through1.address = req.params.through1_address,
				through1.location.coordinates = [parseFloat(req.params.through1_lng), parseFloat(req.params.through1_lat)];
			}
			
			if(req.params.through2_address) {
				through2.address = req.params.through2_address,
				through2.location.coordinates = [parseFloat(req.params.through2_lng), parseFloat(req.params.through2_lat)];
			}
			
			var call = new Call({
				callType: "normal",
				uniqueId: req.params.uniqueId,
				start: {
					address:   req.params.start_address,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.start_lng), parseFloat(req.params.start_lat)]
					}
				},
				through: through,
				through1: through1,
				through2: through2,
				end: {
					address:   req.params.end_address,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.end_lng), parseFloat(req.params.end_lat)]
					}
				},
				user: {
					userId: req.params.userId,
					phone: userInfo.phone,
					address: req.params.user_address,
					city_do: req.params.user_city_do,
					gu_gun:	req.params.user_gu_gun,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.user_lng), parseFloat(req.params.user_lat)]
					}
				},
				driver: {
					driverId: '',
					phone: ''
				},
				money: parseInt(req.params.money),
				paymentOption: req.params.payment_option,
				paymentYn : req.params.payment_option == 'card' ? 'N' : 'Y',
				departureTime: 0,
				estimateTime: 0,
				etc: req.params.etc,
				useReliefMessage: "",
				evaluatedYN: "N",
				reportedUnfairnessYN: "N",
				apiDrivingId : "",
				premiums : "",
				status: req.params.status,
				updatedTime: new Date().getTime(),
				createdTime: new Date().getTime()
			});
			
            call.save(function (err) {
                next(err, call);
            });
        }, function (call, next) {
        	var callInfo = call.getData();
        	
            res.send({ result: error_code.SUCCESS, callInfo: callInfo, time: 5, range: 2, requestCount: 100 });
            
            var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn : callInfo.paymentYn,
				departureTime: callInfo.departureTime,
				estimateTime: callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage : callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "create"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function cancelCall(req, res) {
	if (svr_utils.checkParams(['userId', 'callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, userInfo, _callInfo);
			});
        }, function (userInfo, callInfo, next) {
        	if(!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
	    	}
        	
        	if(callInfo.status != "wait") {
        		res.send({ result: error_code.NOT_FOR_CANCEL });
				return;
        	}
        	
        	if(callInfo.user.userId != req.params.userId) {
        		res.send({ result: error_code.NOT_MATCH_USER_CALL });
				return;
        	}
        	
        	callInfo.status = "cancel";
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
        		if(_err) {
        			next(_err);
        		} else {
        			next(null, userInfo, callInfo); 
        		}
        	});
        }, function(userInfo, callInfo, next) {
        	res.send({ result: error_code.SUCCESS });
        	
        	callInfo = callInfo.getData();
        	userInfo = userInfo.getData();
        	
        	var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn : callInfo.paymentYn,
				departureTime: callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "cancel"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function updateUseReliefMessage(req, res) {
	if (svr_utils.checkParams(['userId', 'callId', 'reliefMessage'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, userInfo, _callInfo);
			});
        }, function (userInfo, callInfo, next) {
        	if(!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
	    	}
        	
        	if(callInfo.status != "start") {
        		res.send({ result: error_code.NOT_START_CALL });
				return;
        	}
        	
        	callInfo.useReliefMessage = req.params.reliefMessage;
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
        		next(_err, userInfo, callInfo); 
        	});
        }, function(userInfo, callInfo, next) {
        	res.send({ result: error_code.SUCCESS });
        	
        	callInfo = callInfo.getData();
        	userInfo = userInfo.getData();
        	
        	var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime: callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "update"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function callHistory(req, res) {
	if (svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			Call.find({ 'user.userId' : req.params.userId, status: 'done'}).exec(function(_err, _callHistoryList) {
				next(_err, userInfo, _callHistoryList);
			});
        }, function (userInfo, callHistoryList, next) {
        	var retCallHistoryList = [];
        	if(callHistoryList && callHistoryList.length > 0) {
        		for(var i = 0; i < callHistoryList.length; i++) {
        			var callInfo = callHistoryList[i].getData();
        			retCallHistoryList.push(callInfo);
        		}
        	}
        	
        	res.send({ result: error_code.SUCCESS, callHistoryList: retCallHistoryList });
        	next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function evaluateDriver(req, res) {
	if (svr_utils.checkParams(['userId', 'callId', 'starNum'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, userInfo, _callInfo);
			});
		}, function(userInfo, callInfo, next) {
			if(callInfo.status != "end" || callInfo.evaluatedYN == "Y") {
        		res.send({ result: error_code.NOT_FOR_DRIVER_EVALUATE });
				return;
        	}
			
			CardariDriver.findById(new ObjectId(callInfo.driver.driverId)).exec(function(_err, _driverInfo) {
				next(_err, userInfo, callInfo, _driverInfo);
			});
        }, function (userInfo, callInfo, driverInfo, next) {
        	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
        	
        	DriverEvaluate.find({callId: callInfo._id, userId: userInfo._id, driverId: driverInfo._id}).exec(function(_err, _driverEvaluateInfo) {
        		next(_err, userInfo, callInfo, driverInfo, _driverEvaluateInfo);
        	});
        }, function(userInfo, callInfo, driverInfo, driverEvaluateInfo, next) {
        	if(driverEvaluateInfo && driverEvaluateInfo.length > 0) {
        		res.send({ result: error_code.DUPLICATE_DRIVER_EVALUATE });
				return;
        	}
        	
        	callInfo.status = "done";
        	callInfo.evaluatedYN = "Y";
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
        		next(null, userInfo, callInfo, driverInfo); 
        	});
        }, function(userInfo, callInfo, driverInfo, next) {
        	callInfo = callInfo.getData();
        	userInfo = userInfo.getData();
        	
        	var driverEvaluateInfo = new DriverEvaluate({
        		driverId:	callInfo.driver.driverId,
        		userId:		userInfo.userId,
        		callId:		callInfo.callId,
        		starNum:	req.params.starNum,
        		createdTime:	new Date().getTime()
        	});
        	
        	driverEvaluateInfo.save(function(_err) {
        		next(null, userInfo, callInfo, driverInfo); 
        	});
        }, function(userInfo, callInfo, driverInfo, next) {
        	res.send({ result: error_code.SUCCESS });
        	
        	var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn : callInfo.paymentYn,
				departureTime: callInfo.departureTime,
				estimateTime: callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "update"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function unfairnessDriver(req, res) {
	if (svr_utils.checkParams(['userId', 'callId', 'unfairness'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			User.findById(new ObjectId(req.params.userId)).exec(next);
		},
		function (userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, userInfo, _callInfo);
			});
		}, function(userInfo, callInfo, next) {
			if(callInfo.reportedUnfairnessYN == "Y") {
        		res.send({ result: error_code.NOT_FOR_DRIVER_UNFAIRNESS });
				return;
        	}
			
			CardariDriver.findById(new ObjectId(callInfo.driver.driverId)).exec(function(_err, _driverInfo) {
				next(_err, userInfo, callInfo, _driverInfo);
			});
        }, function (userInfo, callInfo, driverInfo, next) {
        	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
        	
        	DriverUnfairness.find({callId: callInfo._id, userId: userInfo._id, driverId: driverInfo._id}).exec(function(_err, _driverUnfairness) {
        		next(_err, userInfo, callInfo, driverInfo, _driverUnfairness);
        	});
        }, function(userInfo, callInfo, driverInfo, driverUnfairness, next) {
        	if(driverUnfairness && driverUnfairness.length > 0) {
        		res.send({ result: error_code.DUPLICATE_DRIVER_UNFAIRNESS });
				return;
        	}
        	
        	callInfo.reportedUnfairnessYN = "Y";
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
        		next(null, userInfo, callInfo, driverInfo); 
        	});
        }, function(userInfo, callInfo, driverInfo, next) {
        	callInfo = callInfo.getData();
        	userInfo = userInfo.getData();
        	
        	var driverUnfairnessInfo = new DriverUnfairness({
        		driverId:	callInfo.driver.driverId,
        		userId:		userInfo.userId,
        		callId:		callInfo.callId,
        		unfairness:	req.params.unfairness,
        		createdTime:	new Date().getTime()
        	});
        	
        	driverUnfairnessInfo.save(function(_err) {
        		next(null, userInfo, callInfo, driverInfo); 
        	});
        }, function(userInfo, callInfo, driverInfo, next) {
        	res.send({ result: error_code.SUCCESS });
        	
        	var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn : callInfo.paymentYn,
				departureTime: callInfo.departureTime,
				estimateTime: callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "update"
            });
            
            log_call.save();
            
            next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function calculatePrice(req, res) {
	if (svr_utils.checkParams(['startAddress', 'endAddress', 'stopByAddress'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	res.send({ result: error_code.SUCCESS, price: 20000, peakYn: "N", extraPrice: 0 });
}

function priceOffer(req, res) {
	if (svr_utils.checkParams(['callId', 'userId', 'acceptYn', 'offeredPrice'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (callInfo, next) {
			if (!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if(callInfo.status != "bidding") {
        		res.send({ result: error_code.NOT_FOR_BIDDING });
				return;
        	}
			
			CardariDriver.findById(new ObjectId(callInfo.driver.driverId)).exec(function(_err, driverInfo) {
				next(_err, callInfo, driverInfo);
			});			
		}, function (callInfo, driverInfo, next) {
			if (!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			User.findById(new ObjectId(callInfo.user.userId)).exec(function(_err, userInfo) {
        		next(_err, callInfo, driverInfo, userInfo);
        	});
		}, function (callInfo, driverInfo, userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			if(req.params.acceptYn == "Y") {
				callInfo.money = parseInt(req.params.offeredPrice);
				
				var descBalanceRate = 20 / 100;
				var descBalance = callInfo.money * descBalanceRate;
				if(driverInfo.balance < descBalance) {
					res.send({ result: error_code.NEED_MONEY });
					return;
				}
				
				var logDriverBalance = new _LogDriverBalance({
					driverId : driverInfo._id,
					callId : callInfo._id,
					beforeBalance :	driverInfo.balance,
					afterBalance :	driverInfo.balance - descBalance,
					balance : descBalance * -1,
					reason : "배차 수수료.",
					createdTime : new Date().getTime(),
					logType : "callCatch"
				});
				
				driverInfo.balance = driverInfo.balance - descBalance;
				driverInfo.save(function(_err) {
					if(_err) {
						next(_err);
					} else {
						logDriverBalance.save(function(_err2) {
							next(_err2, callInfo, driverInfo, userInfo);
						});
					}
				});
			} else {
				next(null, callInfo, driverInfo, userInfo);
			}
		}, function (callInfo, driverInfo, userInfo, next) {
			if(req.params.acceptYn == "Y") {
				callInfo.status = "catch";
	        	callInfo.driver.callUpdateTime = new Date().getTime();
	        	callInfo.updatedTime = new Date().getTime();
			} else {
				callInfo.driver = {
					driverId: null,
					phone: null,
					callUpdateTime: null
				};
				
				callInfo.status = "wait";
				callInfo.updatedTime = new Date().getTime();
			}
			
			callInfo.save(function(_err) {
       			next(_err, callInfo, driverInfo, userInfo); 
        	});
		}, function (callInfo, driverInfo, userInfo, next) {
			if(req.params.acceptYn == "Y") {
				var pushMsg = new PushMessage();
				var message = {
					type: 'catch_call',
					callStatus : callInfo.status,
					callId : callInfo._id,
					biddingPrice : callInfo.money,
					driverInfo : (driverInfo.phone.substr(driverInfo.phone.length - 4, 4) + " 기사님") + "," + driverInfo.phone + ",," + driverInfo.starnum + "," + driverInfo.locations.coordinates[1] + "," + driverInfo.locations.coordinates[0]
				};
				
				if(userInfo.platform == "android") {
					pushMsg.sendMessage(message, [userInfo.gcmId], function(_err) {
						if(_err) {
							logger.error(_err);
						} else {
							next(null, callInfo, driverInfo, userInfo);
						}
					});
				} else {
					pushMsg.sendIOSMessage("콜 배차 알림", message, userInfo.gcmId);
					next(null, callInfo, driverInfo, userInfo);
				}
			} else {
				next(null, callInfo, driverInfo, userInfo);
			}
		}, function(callInfo, driverInfo, userInfo, next) {
			var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime: callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime
            });
			
			if(req.params.acceptYn == "Y") {
				log_call.logType = "catch";
			} else {
				log_call.logType = "bidding_cancel";
			}
            
            log_call.save(function(_err) {
            	next(_err);
            });
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) {
			errFunction(err, req, res);
		}
	});
}

function callDetail(req, res) {
	if (svr_utils.checkParams(['callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, _callInfo);
			});
        }, function (callInfo, next) {
        	if(!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
	    	}
        	
        	res.send({ result: error_code.SUCCESS, callDetail: callInfo.getData() });
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function callUpdatePayment(req, res) {
	if (svr_utils.checkParams(['callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, _callInfo);
			});
        }, function (callInfo, next) {
        	if(!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
	    	}
        	
        	callInfo.paymentYn = "Y";
        	callInfo.save(function(_err) {
        		next(_err);
        	});
        }, function(next) {
        	res.send({ result: error_code.SUCCESS });
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

exports.initialize = initialize;
exports.saveCall = saveCall;
exports.cancelCall = cancelCall;
exports.updateUseReliefMessage = updateUseReliefMessage;
exports.callHistory = callHistory;
exports.evaluateDriver = evaluateDriver;
exports.unfairnessDriver = unfairnessDriver;
exports.calculatePrice = calculatePrice;
exports.priceOffer = priceOffer;
exports.callDetail = callDetail;
exports.callUpdatePayment = callUpdatePayment;
