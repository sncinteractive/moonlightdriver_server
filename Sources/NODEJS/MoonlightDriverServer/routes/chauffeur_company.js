var Async							= require('async');
var ChauffeurCompany				= DB.model('ChauffeurCompany');
var BookmarkChauffeurCompany		= DB.model('BookmarkChauffeurCompany');
var _LogBookmarkChauffeurCompany	= DB.model('_LogBookmarkChauffeurCompany');
var User      						= DB.model('User');
var ObjectId						= DB.Types.ObjectId;

function initialize (server) {
	server.post('/chauffeur_company/list', getChauffeurCompanyList);
	server.post('/chauffeur_company/detail', getChauffeurCompanyDetail);
	server.post('/chauffeur_company/update/bookmark', updateChauffeurCompanyBookmark);
}

function getChauffeurCompanyList(req, res) {
	if(svr_utils.checkParams(['userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function (next) {
 			ChauffeurCompany.find().exec(next);
 		},
 		function(chauffeur_company, next) {
 			var retChauffeurCompany = [];
 			if(chauffeur_company) {
 				var companyIds = [];
 				var listLength = chauffeur_company.length;
 				for(var i = 0; i < listLength; i++) {
 					retChauffeurCompany.push(chauffeur_company[i].getData());
 					companyIds.push(chauffeur_company[i]._id);
 				}
 				
 				BookmarkChauffeurCompany.find({chauffeurCompanyId: {'$in' : companyIds}, userId: req.params.userId}).exec(function(err, chauffeur_company_bookmark) {
 	  				if(err) {
 	  					next(err);
 	  				} else {
 	  					var retBookmarkChauffeurCompany = [];
 	  					if(chauffeur_company_bookmark) {
 	  						for(var i = 0; i < chauffeur_company_bookmark.length; i++) {
 	  							retBookmarkChauffeurCompany.push(chauffeur_company_bookmark[i].chauffeurCompanyId);
 	  						}
 	  					}
 	  		 			
 	  		 			res.send({ result: error_code.SUCCESS, chauffeur_company_list: retChauffeurCompany, bookmark_list: retBookmarkChauffeurCompany});
 	  					next();
 	  				}
 	  			});
 			} else {
 				res.send({ result: error_code.SUCCESS, chauffeur_company_list: [], bookmark_list: []});
				next();
 			}
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getChauffeurCompanyDetail(req, res) {
	if(svr_utils.checkParams(['chauffeurCompanyId', 'userId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function (next) {
 			ChauffeurCompany.findById(new ObjectId(req.params.chauffeurCompanyId)).exec(next);
 		},
 		function(chauffeur_company_info, next) {
 			if(!chauffeur_company_info) {
 				res.send({ result: error_code.NO_SUCH_CHAUFFEUR_COMPANY });
				return;
 			}
 			
 			BookmarkChauffeurCompany.findOne({chauffeurCompanyId: req.params.chauffeurCompanyId, userId: req.params.userId}).exec(function(err, chauffeur_company_bookmark) {
  				if(err) {
  					next(err);
  				} else {
  					var retChauffeurCompanyInfo = chauffeur_company_info.getData();
  					retChauffeurCompanyInfo.isBookmark = false;
  					
  					if(chauffeur_company_bookmark) {
  						retChauffeurCompanyInfo.isBookmark = true;
  					}
  		 			
  		 			res.send({ result: error_code.SUCCESS, chauffeur_company_detail: retChauffeurCompanyInfo});
  					next();
  				}
  			});
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function updateChauffeurCompanyBookmark(req, res) {
	if(svr_utils.checkParams(['chauffeurCompanyId', 'userId', 'bookmarkType'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
		
	Async.waterfall([
 		function (next) {
 			User.findById(new ObjectId(req.params.userId)).exec(next);
 		},
 		function(user_info, next) {
 			if(!user_info) {
 				res.send({ result: error_code.NO_SUCH_USER });
				return;
 			}
 			
 			ChauffeurCompany.findById(new ObjectId(req.params.chauffeurCompanyId)).exec(function(err, chauffeur_company) {
 				next(err, chauffeur_company, user_info);
 			});
 		},
 		function(chauffeur_company, user_info, next) {
 			if(!chauffeur_company) {
 				res.send({ result: error_code.NO_SUCH_CHAUFFEUR_COMPANY });
				return;
 			}
 			
 			user_info = user_info.getData();
 			chauffeur_company = chauffeur_company.getData();
 			
 			if(req.params.bookmarkType == "register") {
 				var bookmark_data = new BookmarkChauffeurCompany({
 	 				chauffeurCompanyId: chauffeur_company.chauffeurCompanyId,
 	 				userId : user_info.userId,
 	 				createTime: new Date().getTime()
 	 			});
 	 			
 	 			bookmark_data.save(function(err) {
 	 				next(err, chauffeur_company, user_info);
 	  			});	
 			} else if(req.params.bookmarkType == "unregister") {
 				BookmarkChauffeurCompany.remove({chauffeurCompanyId : req.params.chauffeurCompanyId, userId : req.params.userId}, function(err, data) {
 					next(err, chauffeur_company, user_info);
 				})
 			} else {
 				res.send({ result: error_code.NOT_ENOUGH_PARAMS });
				return;
 			}
 		}, function(chauffeur_company, user_info, next) {
 			res.send({ result: error_code.SUCCESS });
 			
 			var log_chauffeur_company = new _LogBookmarkChauffeurCompany({
 				chauffeurCompanyId:	chauffeur_company.chauffeurCompanyId,
 				userId:			user_info.userId,
 				log_type:		req.params.bookmarkType,
 				logDate: 		new Date()
 			});
	 			
 			log_chauffeur_company.save();
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

exports.initialize = initialize;
exports.getChauffeurCompanyList = getChauffeurCompanyList;
exports.getChauffeurCompanyDetail = getChauffeurCompanyDetail;
exports.updateChauffeurCompanyBookmark = updateChauffeurCompanyBookmark;
