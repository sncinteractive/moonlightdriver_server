var Async			= require('async');
var User	      	= DB.model('User');
var Call			= DB.model('Call');
var CardariDriver	= DB.model('CardariDriver');
var CallHistory 	= DB.model('CallHistory');
var RefusedCall 	= DB.model('RefusedCall');
var _LogCall		= DB.model('_LogCall');
var _LogDriverBalance = DB.model('_LogDriverBalance');
var _LogInsuranceDriving = DB.model('_LogInsuranceDriving');
var ObjectId		= DB.Types.ObjectId;
var PushMessage		= require(rootPath + '/lib/PushMessage');
//var request 		= require('request');

function initialize (server) {
	server.get('/call/list', callList);
	server.get('/call/detail', callDetail);
	server.post('/call/catch', catchCall);
	server.post('/call/refuse', refuseCall);
	server.post('/call/cancel', cancelCall);
	server.post('/call/start', startCall);
	server.post('/call/end', endCall);
	server.post('/call/bidding', biddingCall);
	server.post('/call/bidding/cancel', biddingCancel);
	server.get('/call/history/list', callHistory);
}

// Call List 가져오는 API이다.
// API : HTTP GET /call
function callList (req, res) {
	if (svr_utils.checkParams(['queryType', 'lat', 'lng', 'dist', 'includeRefusal'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			var lat = parseFloat(req.params.lat);
		    var lng = parseFloat(req.params.lng);
		    var dist = parseFloat(req.params.dist);
		    if(dist <= 1000) {
		    	dist = 1000;
		    }
		    
			Call.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": dist,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { "status": "wait" }
				}
			], function(err, calls) {
				next(err, calls);
			});
		}, function (calls, next) {
            var retCalls = [];
            for(var i = 0; i < calls.length; i++) {
            	var retCall = {
            		callId:			calls[i]._id,
            		orderType:		1,
            		refused:		false,
            		clientGrade:	'VIP',
            		uniqueId: 		calls[i].uniqueId,
            		clientLocation:	calls[i].user,
            		startLocation:	calls[i].start,
            		endLocation:	calls[i].end,
            		throughLocationList: [
            			calls[i].through,
            			calls[i].through1,
            			calls[i].through2
            		],
            		paymentOption: calls[i].paymentOption,
            		paymentYn: calls[i].paymentYn,
            		money:		calls[i].money,
            		distance:	calls[i].distance,
            		createdTime:	calls[i].createdTime
            	};
            	
            	retCall.clientLocation.phone = "";
            	retCalls.push(retCall);
            }

			res.send({ result: error_code.SUCCESS, orderDataList: retCalls });
            next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// Call 단일 가져오는 API이다.
// API : HTTP GET /call/:callId
function callDetail (req, res) {
	if (svr_utils.checkParams(['callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			var retCall = {
        		callId:			call._id,
        		orderType:		1,
        		refused:		false,
        		clientGrade:	'VIP',
        		uniqueId: 		call.uniqueId,
        		clientLocation:	call.user,
        		startLocation:	call.start,
        		endLocation:	call.end,
        		driver:			call.driver,
        		throughLocationList: [
        			call.through,
        			call.through1,
        			call.through2
        		],
        		paymentOption: call.paymentOption,
        		paymentYn: call.paymentYn,
        		departureTime : call.departureTime,
        		estimateTime : call.estimateTime,
        		money:		call.money,
        		distance:	call.distance,
        		createdTime:	call.createdTime
        	};

			retCall.userPhone = call.user.phone.substr(call.user.phone.length - 4, call.user.phone.length - 1);
			res.send({ result: error_code.SUCCESS, callDetail: retCall });
		}
	], function (err) {
        if (err) errFunction(err, req, res);
	});
}

// API : HTTP POST /call/catch
function catchCall (req, res) {
	if (svr_utils.checkParams(['driverId', 'callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function(next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
		},
		function (driverInfo, next) {
			if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, _callInfo) {
				next(_err, driverInfo, _callInfo);
			});
        }, function (driverInfo, callInfo, next) {
        	if(!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
	    	}
        	
        	if(callInfo.status != "wait") {
        		res.send({ result: error_code.NOT_FOR_CATCH });
				return;
        	}
        	
        	User.findById(new ObjectId(callInfo.user.userId)).exec(function(_err, _userInfo) {
        		next(_err, driverInfo, callInfo, _userInfo);
        	});
        }, function(driverInfo, callInfo, userInfo, next) {
        	if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
        	
        	var descBalanceRate = 20 / 100;
			var descBalance = callInfo.money * descBalanceRate;
			if(driverInfo.balance < descBalance) {
				res.send({ result: error_code.NEED_MONEY });
				return;
			}
			
			var logDriverBalance = new _LogDriverBalance({
				driverId : driverInfo._id,
				callId : callInfo._id,
				beforeBalance :	driverInfo.balance,
				afterBalance :	driverInfo.balance - descBalance,
				balance : descBalance * -1,
				reason : "배차 수수료.",
				createdTime : new Date().getTime(),
				logType : "callCatch"
			});
			
			driverInfo.balance = driverInfo.balance - descBalance;
			driverInfo.save(function(_err) {
				if(_err) {
					next(_err);
				} else {
					logDriverBalance.save(function(_err2) {
						next(_err2, driverInfo, callInfo, userInfo);
					});
				}
			});
        }, function(driverInfo, callInfo, userInfo, next) {
        	callInfo.status = "catch";
        	callInfo.driver.driverId = req.params.driverId;
        	callInfo.driver.phone = driverInfo.phone;
        	callInfo.driver.callUpdateTime = new Date().getTime();
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
       			next(_err, driverInfo, callInfo, userInfo); 
        	});
        }, function(driverInfo, callInfo, userInfo, next) {
        	var pushMsg = new PushMessage();
			var message = {
				type: 'call_driver_assigned',
				callStatus : callInfo.status,
				callId : callInfo._id,
				driverInfo : (driverInfo.phone.substr(driverInfo.phone.length - 4, 4) + " 기사님") + "," + driverInfo.phone + ",," + driverInfo.starnum + "," + driverInfo.locations.coordinates[1] + "," + driverInfo.locations.coordinates[0]
			};
			
			if(userInfo.platform == "android") {
				pushMsg.sendMessage(message, [userInfo.gcmId], function(_err) {
					if(_err) {
						logger.error(_err);
						next(_err);
					} else {
						next(null, driverInfo, callInfo, userInfo);
					}
				});
			} else {
				pushMsg.sendIOSMessage("배차 완료 알림", message, userInfo.gcmId);
				next(null, driverInfo, callInfo, userInfo);
			}
        }, function(driverInfo, callInfo, userInfo, next) {
        	driverInfo = driverInfo.getData();
        	callInfo = callInfo.getData();
        	userInfo = userInfo.getData();
        	
        	var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "catch"
            });
            
            log_call.save(function(_err) {
            	next(_err);
            });
        }, function(next) {
        	res.send({ result: error_code.SUCCESS });
        	next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

// Call 배차를 Cancel하는 API이다.
// API : HTTP POST /call/cancel
function cancelCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function (driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			User.findById(new ObjectId(call.user.userId)).exec(function (err, user) {
				next(err, driver, call, user);
			});
		}, function (driver, call, user, next) {
			if (!user) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
			}
			
			if(call.status != "catch") {
        		res.send({ result: error_code.NOT_FOR_CANCEL });
				return;
        	}
			
			var descBalanceRate = 20 / 100;
			var descBalance = call.money * descBalanceRate;
			
			var logDriverBalance = new _LogDriverBalance({
				driverId : driver._id,
				callId : call._id,
				beforeBalance :	driver.balance,
				afterBalance :	driver.balance + descBalance,
				balance : descBalance,
				reason : "배차 수수료 환불.",
				createdTime : new Date().getTime(),
				logType : "refundCallCatch"
			});
			
			logDriverBalance.save(function(err) {
				if(err) {
					next(err);
				} else {
					driver.balance = driver.balance + descBalance;
					
					var cancelCallFee = 3000;
					var callUpdateTime = call.driver.callUpdateTime;
					var nowTime = new Date().getTime();
					if(nowTime >= (callUpdateTime + (3 * 60 * 1000))) {
						// 벌금 항목을 여기에 넣어주자.
						driver.balance = driver.balance - cancelCallFee;
						driver.save(function(_err) {
							if(_err) {
								next(_err);
							} else {
								var logDriverBalance2 = new _LogDriverBalance({
									driverId : driver._id,
									callId : call._id,
									beforeBalance :	driver.balance,
									afterBalance :	driver.balance - cancelCallFee,
									balance : cancelCallFee * -1,
									reason : "배차 취소 수수료.",
									createdTime : new Date().getTime(),
									logType : "callCancel"
								});
								
								logDriverBalance2.save(function(_err2) {
									next(_err2, call, user);
								});
							}
						});
					} else {
						next(null, call, user);
					}
				}
			});
		}, function(call, user, next) {
			call.driver = {
				driverId: null,
				phone: null,
				callUpdateTime: null
			};
			
			call.status = "wait";
			call.updatedTime = new Date().getTime();
			
			call.save(function (err) {
				next(err, call, user);
			});
		}, function(call, user, next) {
			var pushMsg = new PushMessage();
			var message = {
				type: 'call_driver_canceled',
				callStatus : call.status,
				callId : call._id
			};
			
			if(user.platform == "android") {
				pushMsg.sendMessage(message, [user.gcmId], function(_err) {
					if(_err) {
						logger.error(_err);
						next(_err);
					} else {
						next(null, call);
					}
				});
			} else {
				pushMsg.sendIOSMessage("배차 취소 알림", message, user.gcmId);
				next(null, call);
			}
		}, function (call, next) {
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				money: callInfo.money,
				byCard: callInfo.byCard,
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "cancel"
            });
            
            log_call.save(function(_err) {
            	next(_err);
            });
        }, function(next) {
        	res.send({ result: error_code.SUCCESS });
 			next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

// Call의 운행 시작을 알리는 API이다.
// API : HTTP POST /call/:callId/start
function startCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var departureTime = req.params.departureTime ? req.params.departureTime : new Date().getTime();
	var estimateTime = req.params.estimateTime ? req.params.estimateTime : 0;
	
	Async.waterfall([
		function(next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if(!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function(_err, call) {
				next(_err, driver, call);
			});
        }, function (driver, call, next) {
        	if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			if (call.status == "wait") {
				res.send({ result: error_code.NOT_CATCH_CALL });
				return;
			} else if(call.status == "catch" && call.paymentYn != 'Y') {
				res.send({ result: error_code.WAITING_FOR_PAYMENT });
				return;
			} else if (call.status == "start") {
				res.send({ result: error_code.START_CALL });
				return;
			} else if (call.status == "end" || call.status == "done") {
				res.send({ result: error_code.CALL_DONE });
				return;
			} else if (call.status == "bidding") {
				res.send({ result: error_code.BIDDING_CALL });
				return;
			}
        	
        	User.findById(new ObjectId(call.user.userId)).exec(function(_err, user) {
        		next(_err, driver, call, user);
        	});
        }, function (driver, call, user, next) {
			if (!user) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
			}
			
			call.status = "start";
			call.departureTime = departureTime;
			call.estimateTime = estimateTime;
			call.driver.callUpdateTime = new Date().getTime();
			call.updatedTime = new Date().getTime();
			
//			call.apiDrivingId = resData.api_driving_id;
//			call.premiums = resData.premiums;
			
			call.save(function (err) {
				next(err, driver, call, user);
			});
			
//			var params = {
//        		apiKey : config.INSURANCE.API_KEY,
//        		driving_mode : "1",
//        		api_driving_id : "",
//        		biz_driving_id : call._id,
//        		api_driver_id : driver.apiDriverId,
//        		call_type : "1",
//        		driver_cell : svr_utils.AES_Encode(driver.phone),
//        		order_maker : "",
//        		order_maker_call : "",
//        		order_receiver : "",
//        		order_receiver_call : "",
//        		start_address : call.start.address,
//        		start_gps : call.start.location.coordinates[1] + "," + call.start.location.coordinates[0],
//        		target_address : call.end.address,
//        		target_gps : call.end.location.coordinates[1] + "," + call.end.location.coordinates[0],
//        		client_cell : call.user.phone.substr(call.user.phone.length - 4, call.user.phone - 1),
//        		client_car_type : "",
//        		client_car_model : "",
//        		driving_time : new Date().getYYYYMMDDHHMMSS()
//        	};
//			
//			saveInsuranceDrivingLog(params, null);
//        	
//        	request.post({
//        		headers: {'content-type' : 'application/x-www-form-urlencoded'},
//        		url:     config.INSURANCE.HOST + config.INSURANCE.DRIVING_URL,
//        		body:    JSON.stringify(params)
//        	}, function(error, response, body) {
//        		console.log(body);
//        		var resData = JSON.parse(body);
//        		
//        		saveInsuranceDrivingLog(params, resData);
//        		
//        		if(resData.Message == "success") {
//        			call.status = "start";
//        			call.departureTime = departureTime;
//        			call.estimateTime = estimateTime;
//        			call.driver.callUpdateTime = new Date().getTime();
//        			call.updatedTime = new Date().getTime();
//        			
//        			call.apiDrivingId = resData.api_driving_id;
//        			call.premiums = resData.premiums;
//        			
//        			call.save(function (err) {
//        				next(err, driver, call, user);
//        			});
//        		} else {
//        			next("api error : " + resData.Message);
//        		}
//        	});
		}, function(driver, call, user, next) {
			var pushMsg = new PushMessage();
			var message = {
				type: 'call_driver_driving',
				callStatus : call.status,
				departureTime : call.departureTime,
				estimateTime : call.estimateTime,
				callId : call._id
			};
			
			if(user.platform == "android") {
				pushMsg.sendMessage(message, [user.gcmId], function(_err) {
					if(_err) {
						logger.error(_err);
						next(_err);
					} else {
						next(null, call);
					}
				});
			} else {
				pushMsg.sendIOSMessage("운행 시작 알림", message, user.gcmId);
				next(null, call);
			}
		}, function (call, next) {
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				apiDrivingId : callInfo.apiDrivingId,
				premiums : callInfo.premiums,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "start"
            });
            
            log_call.save(function(_err) {
            	next(_err);
            });
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// Call의 운행 종료를 알리는 API이다.
// API : HTTP POST /call/:callId/stop
function endCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			Call.findById(new ObjectId(req.params.callId)).exec(function (err, call) {
				next(err, driver, call);
			});
		}, function(driver, call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if (call.driver.driverId != driver._id) {
				res.send({ result: error_code.NOT_PERMITTED });
				return;
			}
			
			if (call.status == "wait") {
				res.send({ result: error_code.NOT_CATCH_CALL });
				return;
			} else if (call.status == "catch") {
				res.send({ result: error_code.NOT_START_CALL });
				return;
			} else if (call.status == "end" || call.status == "done") {
				res.send({ result: error_code.CALL_DONE });
				return;
			} else if (call.status == "bidding") {
				res.send({ result: error_code.BIDDING_CALL });
				return;
			}
			
			User.findById(new ObjectId(call.user.userId)).exec(function(_err, user) {
        		next(_err, driver, call, user);
        	});
		}, function (driver, call, user, next) {
			if (!user) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
			}
			
			call.status = "end";
			
			call.save(function (err) {
				if(err) {
					next(err);
				} else {
					var logDriverBalance = new _LogDriverBalance({
						driverId : driver._id,
						callId : call._id,
						beforeBalance :	driver.balance,
						afterBalance :	driver.balance + call.money,
						balance : call.money,
						reason : "운행 종료.",
						createdTime : new Date().getTime(),
						logType : "callEnd"
					});
					
					driver.balance = driver.balance + call.money;
					driver.save(function(_err) {
						if(_err) {
							next(_err);
						} else {
							logDriverBalance.save(function(_err2) {
								next(_err2, driver, call, user);	
							});
						}
					});
				}
			});
			
//			var params = {
//        		apiKey : config.INSURANCE.API_KEY,
//        		driving_mode : "2",
//        		api_driving_id : call.apiDrivingid,
//        		biz_driving_id : call._id,
//        		api_driver_id : driver.apiDriverId,
//        		call_type : "1",
//        		driver_cell : svr_utils.AES_Encode(driver.phone),
//        		order_maker : "",
//        		order_maker_call : "",
//        		order_receiver : "",
//        		order_receiver_call : "",
//        		start_address : call.start.address,
//        		start_gps : call.start.location.coordinates[1] + "," + call.start.location.coordinates[0],
//        		target_address : call.end.address,
//        		target_gps : call.end.location.coordinates[1] + "," + call.end.location.coordinates[0],
//        		client_cell : call.user.phone.substr(call.user.phone.length - 4, call.user.phone - 1),
//        		client_car_type : "",
//        		client_car_model : "",
//        		driving_time : new Date().getYYYYMMDDHHMMSS()
//        	};
//			
//			saveInsuranceDrivingLog(params, null);
//        	
//        	request.post({
//        		headers: {'content-type' : 'application/x-www-form-urlencoded'},
//        		url:     config.INSURANCE.HOST + config.INSURANCE.DRIVING_URL,
//        		body:    JSON.stringify(params)
//        	}, function(error, response, body) {
//        		console.log(body);
//        		var resData = JSON.parse(body);
//        		
//        		saveInsuranceDrivingLog(params, resData);
//        		
//        		if(resData.Message == "success") {
//        			call.status = "end";
//        			
//        			call.save(function (err) {
//        				if(err) {
//        					next(err);
//        				} else {
//        					var logDriverBalance = new _LogDriverBalance({
//        						driverId : driver._id,
//        						callId : call._id,
//        						beforeBalance :	driver.balance,
//        						afterBalance :	driver.balance + call.money,
//        						balance : call.money,
//        						reason : "운행 종료.",
//        						createdTime : new Date().getTime(),
//        						logType : "callEnd"
//        					});
//        					
//        					driver.balance = driver.balance + call.money;
//        					driver.save(function(_err) {
//        						if(_err) {
//        							next(_err);
//        						} else {
//        							logDriverBalance.save(function(_err2) {
//        								next(_err2, driver, call, user);	
//        							});
//        						}
//        					});
//        				}
//        			});
//        		} else {
//        			next("api_error : " + resData.Message);
//        		}
//        	});
		}, function(driver, call, user, next) {
			var pushMsg = new PushMessage();
			var message = {
				type: 'call_driver_arrived_target',
				callStatus : call.status,
				callId: call._id
			};
			
			if(user.platform == "android") {
				pushMsg.sendMessage(message, [user.gcmId], function(_err) {
					if(_err) {
						logger.error(_err);
						next(_err);
					} else {
						next(null, call);
					}
				});
			} else {
				pushMsg.sendIOSMessage("운행 종료 알림", message, user.gcmId);
				next(null, call);
			}
		}, function (call, next) {
			var callInfo = call.getData();
			
			var log_call = new _LogCall({
				callId: callInfo.callId,
				callType: callInfo.callType,
				status: callInfo.status,
				uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "end"
            });
            
            log_call.save(function(_err) {
            	next(_err);
            });
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function callHistory(req, res) {
	if (svr_utils.checkParams(['selectedType'], req.params)) {
        res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
    }

	var lastMonth;
	var thirdLastMonth;

	Async.waterfall([
			function(next) {
				CallHistory.aggregate([
					{$group: {_id: '$date'}},
					{$sort: {_id: -1}},
					{ $limit : 3 }
				], function(err, recentDate) {
					next(err, recentDate);
				});
			},
			function(recentDate, next) {
				lastMonth = recentDate[0]["_id"];
				thirdLastMonth = recentDate[2]["_id"];
				
				var gu_ret = {};
				var dong_ret = {};
				var selectedType = req.params.selectedType;
				var paramMonth = 0;
				if(selectedType == 0) {
					paramMonth = lastMonth;
				} else if(selectedType == 1) {
					paramMonth = thirdLastMonth;
				} else if(selectedType == 2) {
                    var searchYear = new Date().getFullYear() - 1;
					var searchMonth = new Date().getMonth() + 1;
					if(searchMonth < 10) {
                        searchMonth = "0" + searchMonth;
                    }

                    paramMonth = searchYear + "" + searchMonth;
                }
				
				CallHistory.aggregate([
						{
							"$match": {
								"date" : { "$gte": paramMonth }
							}
						},
						{
							"$group": {
								"_id": {
									"si_name": "$si_name",
									"gu_name": "$gu_name",
									"dong_name": "$dong_name"
								},
								"call_count": { "$avg": "$call_count" },
								"count": { "$sum": 1 }
							}
						}
					], function(err, history) {
						next(err, history);
					}
				);
			},
			function(_history, next) {
				var history_count = _history.length;
				var gu_ret = {};
				var dong_ret = {};
				for(var i = 0; i < history_count; i++) {
					var gu_name = _history[i]._id.gu_name;
					var dong_name = gu_name + "_" + _history[i]._id.dong_name;
					
					if(!dong_ret[dong_name]) {
						dong_ret[dong_name] = {
							si_name: _history[i]._id.si_name,
							gu_name: _history[i]._id.gu_name,
							dong_name: _history[i]._id.dong_name,
							call_count: 0
						};
					}
					
					if(!gu_ret[gu_name]) {
						gu_ret[gu_name] = {
							si_name: _history[i]._id.si_name,
							gu_name: _history[i]._id.gu_name,
							call_count: 0
						};
					}

					dong_ret[dong_name].call_count = _history[i].call_count;
					gu_ret[gu_name].call_count = gu_ret[gu_name].call_count + _history[i].call_count;
				}

				res.send({result: error_code.SUCCESS, dong_data: dong_ret, gu_data: gu_ret, total_count: history_count});
				next();
			}
		], function(err) {
			if(err) {
				errFunction(err, req, res);
			}
		}
	);
}

function refuseCall (req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}

	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (call, next) {
			if (!call) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(function(_err, _driverInfo) {
				next(_err, call, _driverInfo);
			});			
		}, function (_call, _driverInfo, next) {
			if (!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			RefusedCall.findOne({driverId : req.params.driverId, callId : req.params.callId}).exec(function(_err, _refusedCall) {
				next(_err, _call, _driverInfo, _refusedCall);
			});
		}, function (_call, _driverInfo, _refusedCall, next) {
			if (_refusedCall) {
				res.send({ result: error_code.SUCCESS });
				return;
			}
			
			var refusedCall = new RefusedCall({
				driverId : req.params.driverId,
				callId : req.params.callId,
				createdTime : new Date().getTime()
			});
			
			refusedCall.save(function(_err) {
				next(_err);
			});
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) {
			errFunction(err, req, res);
		}
	});
}

function biddingCall(req, res) {
	if (svr_utils.checkParams(['callId', 'biddingPrice', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (callInfo, next) {
			if (!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if(callInfo.status != "wait") {
        		res.send({ result: error_code.NOT_FOR_BIDDING });
				return;
        	}
			
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(function(_err, driverInfo) {
				next(_err, callInfo, driverInfo);
			});			
		}, function (callInfo, driverInfo, next) {
			if (!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			User.findById(new ObjectId(callInfo.user.userId)).exec(function(_err, userInfo) {
        		next(_err, callInfo, driverInfo, userInfo);
        	});
		}, function (callInfo, driverInfo, userInfo, next) {
			if(!userInfo) {
				res.send({ result: error_code.NO_SUCH_USER });
				return;
	    	}
			
			var biddingPrice = parseInt(req.params.biddingPrice);
			var descBalanceRate = 20 / 100;
			var descBalance = biddingPrice * descBalanceRate;
			if(driverInfo.balance < descBalance) {
				res.send({ result: error_code.NEED_MONEY });
				return;
			}
			
			callInfo.status = "bidding";
        	callInfo.driver.driverId = req.params.driverId;
        	callInfo.driver.phone = driverInfo.phone;
        	callInfo.driver.callUpdateTime = new Date().getTime();
        	callInfo.updatedTime = new Date().getTime();
        	
        	callInfo.save(function(_err) {
       			next(_err, callInfo, driverInfo, userInfo); 
        	});
		}, function (callInfo, driverInfo, userInfo, next) {
			var pushMsg = new PushMessage();
			var message = {
				type: 'call_driver_price_offer',
				callStatus : callInfo.status,
				offeredPrice: req.params.biddingPrice,
				callId : callInfo._id,
				driverInfo : (driverInfo.phone.substr(driverInfo.phone.length - 4, 4) + " 기사님") + "," + driverInfo.phone + ",," + driverInfo.starnum + "," + driverInfo.locations.coordinates[1] + "," + driverInfo.locations.coordinates[0]
			};
			
			if(userInfo.platform == "android") {
				pushMsg.sendMessage(message, [userInfo.gcmId], function(_err) {
					next(_err, callInfo, driverInfo, userInfo);
				});
			} else {
				pushMsg.sendIOSMessage("콜비 수정 알림", message, userInfo.gcmId);
				next(null, callInfo, driverInfo, userInfo);
			}
		}, function(callInfo, driverInfo, userInfo, next) {
			var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn: callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "bidding"
            });
            
            log_call.save(function(_err) {
            	next(_err);
            });
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) {
			errFunction(err, req, res);
		}
	});
}

function biddingCancel(req, res) {
	if (svr_utils.checkParams(['callId', 'driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
		function (next) {
			Call.findById(new ObjectId(req.params.callId)).exec(next);
		}, function (callInfo, next) {
			if (!callInfo) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			if(callInfo.status != "bidding") {
        		res.send({ result: error_code.NOT_FOR_CATCH });
				return;
        	}
			
			CardariDriver.findById(new ObjectId(callInfo.driver.driverId)).exec(function(_err, driverInfo) {
				next(_err, callInfo, driverInfo);
			});			
		}, function (callInfo, driverInfo, next) {
			if (!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
			
			callInfo.driver = {
				driverId: null,
				phone: null,
				callUpdateTime: null
			};
			
			callInfo.status = "wait";
			callInfo.updatedTime = new Date().getTime();
			callInfo.save(function(_err) {
       			next(_err, callInfo, driverInfo); 
        	});
		}, function(callInfo, driverInfo, next) {
			var log_call = new _LogCall({
            	callId: callInfo.callId,
            	callType: callInfo.callType,
            	status: callInfo.status,
            	uniqueId: callInfo.uniqueId,
				start: {
					address:   callInfo.start.address,
					location: {
						type: callInfo.start.location.type,
						coordinates: callInfo.start.location.coordinates
					}
				},
				through: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through1: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				through2: {
					address: callInfo.through.address,
					location: {
						type: callInfo.through.location.type,
						coordinates: callInfo.through.location.coordinates
					}
				},
				end: {
					address: callInfo.end.address,
					location: {
						type: callInfo.end.location.type,
						coordinates: callInfo.end.location.coordinates
					}
				},
				user: {
					userId: callInfo.user.userId,
					phone: callInfo.user.phone,
					address: callInfo.user.user_address,
					city_do: callInfo.user.user_city_do,
					gu_gun:	callInfo.user.user_gu_gun,
					location: {
						type: callInfo.user.location.type,
						coordinates: callInfo.user.location.coordinates
					}
				},
				driver: {
					driverId: callInfo.driver.driverId,
					phone: callInfo.driver.phone,
					callUpdateTime: callInfo.driver.callUpdateTime
				},
				money: parseInt(callInfo.money),
				paymentOption: callInfo.paymentOption,
				paymentYn : callInfo.paymentYn,
				departureTime : callInfo.departureTime,
				estimateTime : callInfo.estimateTime,
				etc: callInfo.etc,
				useReliefMessage: callInfo.useReliefMessage,
				evaluatedYN: callInfo.evaluatedYN,
				reportedUnfairnessYN: callInfo.reportedUnfairnessYN,
				updatedTime: callInfo.updatedTime,
				createdTime: callInfo.createdTime,
				logType: "bidding_cancel"
            });
			
            log_call.save(function(_err) {
            	next(_err);
            });
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function (err) {
		if (err) {
			errFunction(err, req, res);
		}
	});
}

function saveInsuranceDrivingLog(reqParams, resData) {
	var log = new _LogInsuranceDriving({
		callId:	reqParams.biz_driving_id,
		drivingMode: reqParams.driving_mode,
		apiDrivingId: reqParams.api_driving_id,
		apiDriverId: reqParams.api_driver_id,
		bizDrivingId: reqParams.biz_driver_id,
		callType: reqParams.call_type,
		driverCell:	reqParams.driver_cell,
		orderMaker:	reqParams.order_maker,
		orderMakerCall: reqParams.order_maker_call,
		orderReceiver: reqParams.order_receiver,
		orderReceiverCall: reqParams.order_receiver_call,
		startAddress: reqParams.start_address,
		startGps: reqParams.start_gps,
		targetAddress: reqParams.target_address,
		targetGps: reqParams.target_gps,
		clientCell: reqParams.client_cell,
		clientCarType: reqParams.client_car_type,
		clientCarModel: reqParams.client_car_model,
		drivingTime: reqParams.driving_time,
		message: "",
		premiums: "",
	});
	
	if(resData) {
		log.message = resData.Message;
		log.premiums = resData.premiums;
	}
	
	log.save();
}

exports.initialize = initialize;
exports.callList = callList;
exports.callDetail = callDetail;
exports.catchCall = catchCall;
exports.cancelCall = cancelCall;
exports.startCall = startCall;
exports.endCall = endCall;
exports.biddingCall = biddingCall;
exports.callHistory = callHistory;
exports.refuseCall = refuseCall;
exports.biddingCancel = biddingCancel;
