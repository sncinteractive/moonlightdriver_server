var Async					= require('async');
var Call					= DB.model('Call');
var CardariDriver			= DB.model('CardariDriver');
var CardariVersion 			= DB.model('CardariVersion');
var InsuranceCompany 		= DB.model('InsuranceCompany');
var _LogCardariDriver		= DB.model('_LogCardariDriver');
var _LogInsuranceCompany 	= DB.model('_LogInsuranceCompany');
var _LogDriverBalance		= DB.model('_LogDriverBalance');
var _LogCall				= DB.model('_LogCall');
var _LogUnderWriting		= DB.model('_LogUnderWriting');
var _LogInsuranceDriving	= DB.model('_LogInsuranceDriving');
var ObjectId				= DB.Types.ObjectId;

function initialize (server) {
	server.post('/cardari_driver/register/auto', tempRegisterCardariDriver);
	server.post('/cardari_driver/check/register', checkCardaiRegister);
	server.post('/cardari_driver/register', registerCardariDriver);
	server.post('/cardari_driver/login', loginCardariDriver);
	server.post('/cardari_driver/:driverId/edit', editCardariDriver);
	server.post('/cardari_driver/update/confirm', updateCardariDriverConfirmData);
	server.post('/cardari_driver/check', checkCardariDriverConfirmData);
	server.post('/cardari_driver/logout', cardariLogOut);
	server.post('/cardari_driver/update/location', updateCardariDriverLocation);
	server.post('/cardari_insurance/company/insert', cardariInsertInsuranceCompany);
	server.post('/cardari/version', checkCardariVersion);
	server.post('/cardari_driver/income_list', income_list);
	server.post('/cardari_driver/deposit_and_withdraw_list', deposit_and_withdraw_list);
	server.post('/cardari_driver/last_order_list', last_order_list);
	server.post('/cardari_driver/req/underwritinglog', saveReqUnderWritingLog);
	server.post('/cardari_driver/req/drivinglog', saveReqDrivingLog);
}

function checkCardariVersion(req, res) {
	CardariVersion.findOne({version: req.params.version}).exec(function (err, version) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(version) {
				var versionInfo = version.getData();
				
				if(versionInfo.isValid) {
					res.send({ result: error_code.SUCCESS });
				} else {
					res.send({ result: error_code.NEED_UPDATE_VERSION });
				}
			} else {
				res.send({ result: error_code.NOT_ALLOW_VERSION });
			}
		}
	});
}

function checkCardaiRegister(req, res) {
	CardariDriver.findOne({ phone: req.params.phone }).exec(function(err, driverInfo) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if (!driverInfo) {
				res.send({ result: error_code.NOT_REGISTERED_DRIVER });
				return;
			}
	    	
			if(driverInfo.driverStatus == "success" && (driverInfo.availableDate != null && driverInfo.availableDate <= new Date())) {
	    		res.send({ result: error_code.SUCCESS });
	    	} else {
	    		res.send({ result: error_code.FAIL_TO_LOGIN });	    		
	    	}
		}
	});
}

function checkCardariDriverConfirmData(req, res) {
	if(svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(_driverInfo, next) {
	    	if(!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	} else {
	    		var driver = _driverInfo.getData();
	    		
	    		res.send({ result: error_code.SUCCESS });
    			next();
    			
    			// 오픈 일주일 후에 주석 풀어서 기사 등록한 사용자만 셔틀 노선 확인 가능하도록 활성화
//	    		if((driver.name == null || driver.name.length <= 0) || (driver.driverLicense == null || driver.driverLicense.length <= 0) ||
//	    				(driver.insuranceCompanyId == null || driver.insuranceCompanyId.length <= 0) || (driver.insuranceNumber == null || driver.insuranceNumber.length <= 0)) {
//	    			InsuranceCompany.find().exec(function(err, _companyList) {
//	    	    		if(err) {
//	    	    			next(err);
//	    	    		} else {
//	    	    			var companyList = [];
//	    	    			var listSize = _companyList.length;
//	    	    			for(var i = 0; i < listSize; i++) {
//	    	    				companyList.push(_companyList[i].getData());
//	    	    			}
//	    	    			
//	    	    			res.send({ result : error_code.NOT_CONFIRM_DRIVER, company_list : companyList });
//	    	    			return;
//	    	    		}
//	    	    	});
//	    		} else {
//	    			res.send({ result: error_code.SUCCESS });
//	    			next();
//	    		}
	    	}
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function tempRegisterCardariDriver(req, res) {
	if(svr_utils.checkParams(['phone', 'macAddr', 'gcmId', "lat", "lng"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var isRegister = true;
	var lat = req.params.lat ? parseFloat(req.params.lat) : 0;
	var lng = req.params.lng ? parseFloat(req.params.lng) : 0;
	var version = req.params.version ? req.params.version : "";
	
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findOne({ phone: req.params.phone }).exec(next);
	    }, function(_driverInfo, next) {
	    	var driver = null;
	    	if(!_driverInfo) {
	    		isRegister = true;
	    		driver = new CardariDriver({
					gcmId: req.params.gcmId,
					phone: req.params.phone,
					macAddr: req.params.macAddr,
					version: version,
					name: "",
					driverLicense: "",
					insuranceCompanyId: "",
					insuranceNumber: "",
					star: 0,
					ynLogin: "Y",
					starnum: 0,
					balance: 0,
					locations: {
						type: "Point",
						coordinates: [ lng, lat ]
					},
					createdDate : new Date(),
					updatedDate : new Date(),
					lastLoginDate : new Date()
				});
	    	} else {
	    		isRegister = false;
	    		driver = _driverInfo;
	    		driver.ynLogin = "Y";
	    		driver.version = req.params.version;
	    		driver.macAddr = req.params.macAddr;
	    		driver.lastLoginDate = new Date();
	    		driver.locations = {
					type: "Point",
					coordinates: [ lng, lat ]
				};
	    		
	    		if(req.params.gcmId) {
	    			driver.gcmId = req.params.gcmId;
	    		}
	    		
	    		if(!driver.balance) {
	    			driver.balance = 0;
	    		}
	    	}
	    	
	    	driver.save(function(err) {
				next(err, driver);
			});
	    }, function(_driver, next) {
	    	var driverInfo = _driver.getData();
	    	
	    	Call.find({ 'driver.driverId' : driverInfo.driverId, status: {'$ne' : 'done'}}).exec(function(_err, _callInfo) {
				next(_err, driverInfo, _callInfo);
			});
		}, function(_driver, _callInfo, next) {
			if(_callInfo) {
				_callInfo = _callInfo.getData();
			}
			
			var log_driver = new _LogCardariDriver({
				driverId: driverInfo.driverId,
				gcmId: driverInfo.gcmId,
				phone: driverInfo.phone,
				macAddr: driverInfo.macAddr,
				ynLogin: driverInfo.ynLogin,
				locations: driverInfo.locations,
				logType: "reg_auto",
				logDate: new Date()
			});
			
			if(!isRegister) {
				log_driver.logType = "login";
			}
			
			log_driver.save(function(err) {
				next(err, driverInfo, _callInfo);
			});
		}, function(driverInfo, _callInfo, next) {
			res.send({ result: error_code.SUCCESS, driver: driverInfo, callInfo: _callInfo });
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function registerCardariDriver (req, res) {
	var essentialParams = ["uniqueId", "phone", "phone1", "name", "socialNumber", "licenseType", "licenseNumber", "version", "lat", "lng"];
	if (svr_utils.checkParams(essentialParams, req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var isRegister = true;
	var lat = req.params.lat ? parseFloat(req.params.lat) : 0;
	var lng = req.params.lng ? parseFloat(req.params.lng) : 0;
	var version = req.params.version ? req.params.version : "";
	
	Async.waterfall([
		function (next) {
			var driver = new CardariDriver({
				phone : req.params.phone,
				phone1:	req.params.phone1,
				uniqueId : req.params.uniqueId,
				name: req.params.name,
				socialNumber : req.params.socialNumber,
				licenseType : req.params.licenseType,
				licenseNumber : req.params.licenseNumber,
				driverStatus : "wait",
				insuranceStatus : "wait",
				apiDriverId: "",
				insureNumber: "",
				email : req.params.email,
				balance : 0,
				gcmId : "",
				version : version,
				locations : {
					type : "Point",
					coordinates : [ lng, lat ]
				},
				image : "",
				point : 0,
				ynLogin : "N",
				star : 0,
				starnum : 0,
				macAddr : req.params.macAddr,
				createdDate : new Date(),
				updatedDate : new Date(),
				availableDate: '',
				lastLoginDate : ''
			});

			driver.save(function (err) {
				next(err, driver);
			});
		}, function (driver, next) {
			var driverInfo = driver.getData();
			var log_driver = new _LogCardariDriver({
				driverId : driverInfo.driverId,
				phone : driverInfo.phone,
				phone1:	driverInfo.phone1,
				uniqueId : driverInfo.uniqueId,
				name: driverInfo.name,
				socialNumber : driverInfo.socialNumber,
				licenseType : driverInfo.licenseType,
				licenseNumber : driverInfo.licenseNumber,
				driverStatus : driverInfo.driverStatus,
				insuranceStatus : driverInfo.insuranceStatus,
				apiDriverId : driverInfo.apiDriverId,
				insureNumber : driverInfo.insureNumber,
				email : driverInfo.email,
				balance : driverInfo.balance,
				gcmId : driverInfo.gcmId,
				version : driverInfo.version,
				locations : driverInfo.locations,
				image : driverInfo.image,
				point : driverInfo.point,
				ynLogin : driverInfo.ynLogin,
				star : driverInfo.star,
				starnum : driverInfo.starnum,
				macAddr : driverInfo.macAddr,
				logType: "reg_full",
				logDate: new Date()
			});
			
			log_driver.save(function(err) {
				next(err, driverInfo);	
			});
		}, function(driverInfo, next) {
			res.send({ result: error_code.SUCCESS, driverId : driverInfo.driverId });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function loginCardariDriver(req, res) {
	if(svr_utils.checkParams(['phone', 'gcmId', "lat", "lng"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var isRegister = true;
	var lat = req.params.lat ? parseFloat(req.params.lat) : 0;
	var lng = req.params.lng ? parseFloat(req.params.lng) : 0;
	var version = req.params.version ? req.params.version : "";
	
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findOne({ phone: req.params.phone }).exec(next);
	    }, function(_driverInfo, next) {
	    	if (!_driverInfo) {
				res.send({ result: error_code.NOT_REGISTERED_DRIVER });
				return;
			}
	    	
	    	if(_driverInfo.driverStatus != "success") {
	    		res.send({ result: error_code.FAIL_TO_LOGIN });
				return;
	    	}
	    	
	    	driver = _driverInfo;
    		driver.ynLogin = "Y";
    		driver.version = req.params.version;
    		driver.macAddr = req.params.macAddr;
    		driver.lastLoginDate = new Date();
    		driver.locations = {
				type: "Point",
				coordinates: [ lng, lat ]
			};
    		
    		if(req.params.gcmId) {
    			driver.gcmId = req.params.gcmId;
    		}
	    	
	    	driver.save(function(err) {
				next(err, driver);
			});
	    }, function(_driver, next) {
	    	var driverInfo = _driver.getData();
	    	
	    	Call.findOne({ 'driver.driverId' : driverInfo.driverId, status: {'$in' : ['catch', 'start']}}).exec(function(_err, callInfo) {
				next(_err, driverInfo, callInfo);
			});
		}, function(driverInfo, callInfo, next) {
			if(callInfo) {
				callInfo = callInfo.getData();
			}
			
			var log_driver = new _LogCardariDriver({
				driverId : driverInfo.driverId,
				phone : driverInfo.phone,
				phone1:	driverInfo.phone1,
				uniqueId : driverInfo.uniqueId,
				name: driverInfo.name,
				socialNumber : driverInfo.socialNumber,
				licenseType : driverInfo.licenseType,
				licenseNumber : driverInfo.licenseNumber,
				driverStatus : driverInfo.driverStatus,
				insuranceStatus : driverInfo.insuranceStatus,
				apiDriverId : driverInfo.apiDriverId,
				insureNumber : driverInfo.insureNumber,
				email : driverInfo.email,
				balance : driverInfo.balance,
				gcmId : driverInfo.gcmId,
				version : driverInfo.version,
				locations : driverInfo.locations,
				image : driverInfo.image,
				point : driverInfo.point,
				ynLogin : driverInfo.ynLogin,
				star : driverInfo.star,
				starnum : driverInfo.starnum,
				macAddr : driverInfo.macAddr,
				logType: "login",
				logDate: new Date()
			});
			
			log_driver.save(function(err) {
				next(err, driverInfo, callInfo);
			});
		}, function(driverInfo, callInfo, next) {
			res.send({ result: error_code.SUCCESS, driver: driverInfo, callInfo: callInfo });
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function cardariLogOut(req, res) {
	if(svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
		
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(_driverInfo, next) {
	    	if (!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
	    	
	    	if(_driverInfo.ynLogin == "N") {
	    		res.send({ result: error_code.SUCCESS });
	    		return;
	    	}
	    	
	    	_driverInfo.ynLogin = "N";
	    	
	    	_driverInfo.save(function(err) {
	    		if(err) {
	    			next(err);
	    		} else {
	    			var driverInfo = _driverInfo.getData();
	    			
	    			var log_driver = new _LogCardariDriver({
	    				driverId : driverInfo.driverId,
	    				phone : driverInfo.phone,
	    				phone1:	driverInfo.phone1,
	    				uniqueId : driverInfo.uniqueId,
	    				name: driverInfo.name,
	    				socialNumber : driverInfo.socialNumber,
	    				licenseType : driverInfo.licenseType,
	    				licenseNumber : driverInfo.licenseNumber,
	    				driverStatus : driverInfo.driverStatus,
	    				insuranceStatus : driverInfo.insuranceStatus,
	    				apiDriverId : driverInfo.apiDriverId,
	    				insureNumber : driverInfo.insureNumber,
	    				email : driverInfo.email,
	    				balance : driverInfo.balance,
	    				gcmId : driverInfo.gcmId,
	    				version : driverInfo.version,
	    				locations : driverInfo.locations,
	    				image : driverInfo.image,
	    				point : driverInfo.point,
	    				ynLogin : driverInfo.ynLogin,
	    				star : driverInfo.star,
	    				starnum : driverInfo.starnum,
	    				macAddr : driverInfo.macAddr,
	    				logType: "log_out",
	    				logDate: new Date()
	    			});
	    			
	    			log_driver.save(function(err) {
	    				next(err);
	    			});
	    		}
			});
	    }, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function updateCardariDriverConfirmData(req, res) {
	if(svr_utils.checkParams(['driverId', 'name', 'driverLicense', 'insuranceCompanyId', 'insuranceNumber'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    },
	    function(_driverInfo, next) {
	    	if (!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}
	    	
	    	CardariDriver.find({insuranceNumber: req.params.insuranceNumber}).exec(function (err, list) {
	    		if(err) {
	    			next(err);
	    		} else {
	    			next(null, _driverInfo, list);
	    		}
	    	});
	    }, function(_driverInfo, _list, next) {
	    	if(_list.length > 0) {
	    		res.send({ result: error_code.DUPLICATE_INSURANCE_NUMBER });
				return;
	    	}
	    	
	    	InsuranceCompany.findById(new ObjectId(req.params.insuranceCompanyId)).exec(function(err, _company) {
	    		if(err) {
	    			next(err);
	    		} else {
	    			next(null, _driverInfo, _company);
	    		}
	    	});
	    }, function(_driverInfo, _company, next) {
	    	if(!_company) {
	    		res.send({ result: error_code.NOT_ALLOW_INSURANCE_COMPANY });
				return;
	    	}
	    	
	    	var regExp = new RegExp('^\\d{2}[-]\\d{2}[-]\\d{6}[-]\\d{2}$', 'g');
	    	if(!regExp.test(req.params.driverLicense)) {	// 면허번호 체크
	    		res.send({ result: error_code.NOT_ALLOW_DRIVER_LICENSE });
				return;
	    	}
	    	
	    	regExp = new RegExp(_company.regExp, 'g');
	    	if(!regExp.test(req.params.insuranceNumber)) {	// 보험번호 체크
	    		res.send({ result: error_code.NOT_ALLOW_INSURANCE_NUMBER });
				return;
	    	} else {
	    		var nowYear = new Date().getFullYear();
	    		var splitStr = req.params.insuranceNumber.split("-");

	    		if(splitStr[0].length == 4) {
	    			var compYear = parseInt(splitStr[0]);
	    			if(nowYear < compYear) {
	    				res.send({ result: error_code.NOT_ALLOW_INSURANCE_NUMBER });
	    				return;
	    			}
	    		}
	    	}
	    	
	    	_driverInfo.name = req.params.name;
	    	_driverInfo.driverLicense = req.params.driverLicense;
	    	_driverInfo.insuranceCompanyId = req.params.insuranceCompanyId;
	    	_driverInfo.insuranceNumber = req.params.insuranceNumber;
	    	_driverInfo.updatedDate = new Date();
	    	
	    	_driverInfo.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, driver: _driverInfo.getData() });
					next(null, _driverInfo);
				}
			});
		}, function(_driver, next) {
			if(_driver != null) {
				var driverInfo = _driver.getData();
				var log_driver = new _LogCardariDriver({
    				driverId : driverInfo.driverId,
    				phone : driverInfo.phone,
    				phone1:	driverInfo.phone1,
    				uniqueId : driverInfo.uniqueId,
    				name: driverInfo.name,
    				socialNumber : driverInfo.socialNumber,
    				licenseType : driverInfo.licenseType,
    				licenseNumber : driverInfo.licenseNumber,
    				driverStatus : driverInfo.driverStatus,
    				insuranceStatus : driverInfo.insuranceStatus,
    				apiDriverId : driverInfo.apiDriverId,
    				insureNumber : driverInfo.insureNumber,
    				email : driverInfo.email,
    				balance : driverInfo.balance,
    				gcmId : driverInfo.gcmId,
    				version : driverInfo.version,
    				locations : driverInfo.locations,
    				image : driverInfo.image,
    				point : driverInfo.point,
    				ynLogin : driverInfo.ynLogin,
    				star : driverInfo.star,
    				starnum : driverInfo.starnum,
    				macAddr : driverInfo.macAddr,
    				logType: "update_confirm",
    				logDate: new Date()
    			});
				
				log_driver.save();	
			}
			
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}


function editCardariDriver (req,res) {
	if (svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
	}
	
	Async.waterfall([
		function (next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				// TODO 예외처리를 해주자.
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
			}

			if (req.params.hasOwnProperty("address")) driver.address = req.params.address;
			if (req.params.hasOwnProperty("worksAt")) driver.worksAt = req.params.worksAt;
			if (req.params.hasOwnProperty("email")) driver.email = req.params.email;
			if (req.params.hasOwnProperty("insurance")) driver.insurance = req.params.insurance;
			if (req.params.hasOwnProperty("phone")) driver.phone = req.params.phone;
			if (req.params.hasOwnProperty("name")) driver.name = req.params.name;
			if (req.params.hasOwnProperty("birth")) driver.birth = req.params.birth;
			if (req.params.hasOwnProperty("gender")) driver.gender = req.params.gender;
			if (req.params.hasOwnProperty("macAddr")) driver.macAddr = req.params.macAddr;
			if (req.params.hasOwnProperty("image")) driver.image = req.params.image;
			if (req.params.hasOwnProperty("isvaild")) driver.isVaild = req.params.isVaild;
			if (req.params.hasOwnProperty("number")) driver.cert.number = req.params.number;
			if (req.params.hasOwnProperty("automatic")) driver.cert.automatic = req.params.automatic;

			driver.save(function (err) {
				if (err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS, driver: driver.getData() });
					next(null, driver);
				}
			});
		}, function(_driver, next) {
			var driverInfo = _driver.getData();
			var log_driver = new _LogCardariDriver({
				driverId : driverInfo.driverId,
				phone : driverInfo.phone,
				phone1:	driverInfo.phone1,
				uniqueId : driverInfo.uniqueId,
				name: driverInfo.name,
				socialNumber : driverInfo.socialNumber,
				licenseType : driverInfo.licenseType,
				licenseNumber : driverInfo.licenseNumber,
				driverStatus : driverInfo.driverStatus,
				insuranceStatus : driverInfo.insuranceStatus,
				apiDriverId : driverInfo.apiDriverId,
				insureNumber : driverInfo.insureNumber,
				email : driverInfo.email,
				balance : driverInfo.balance,
				gcmId : driverInfo.gcmId,
				version : driverInfo.version,
				locations : driverInfo.locations,
				image : driverInfo.image,
				point : driverInfo.point,
				ynLogin : driverInfo.ynLogin,
				star : driverInfo.star,
				starnum : driverInfo.starnum,
				macAddr : driverInfo.macAddr,
				logType: "edit",
				logDate: new Date()
			});
			
			log_driver.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function cardariInsertInsuranceCompany(req, res) {
	if (svr_utils.checkParams(['companyName', 'insuranceNumberFormat'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
        return;
	}
	
	Async.waterfall([
 		function (next) {
 			InsuranceCompany.find({companyName: req.params.companyName}).exec(next);
 		}, function (company, next) {
 			if (company.length > 0) {
 				res.send({ result: error_code.DUPLICATE_INSURANCE_COMPANY });
				return;
 			} else {
 				var company = new InsuranceCompany({
 					companyName: req.params.companyName,
 					numberFormat : req.params.insuranceNumberFormat,
 					regExp : req.params.regExp
 				});

 				company.save(function (err) {
 					if(err) {
 						next(err);
 					} else {
 						res.send({ result: error_code.SUCCESS });
 						next(null, company);
 					}
 				});
 			}
 		}, function(company, next) {
 			var log_company = new _LogInsuranceCompany({
 				companyName: company.companyName,
 				numberFormat: company.insuranceNumberFormat,
 				regExp : company.regExp,
 				logType: "regist",
 				logDate: new Date()
 			});
 			
 			log_company.save();
 			
 			next();
 		}
 	], function (err) {
 		if (err) errFunction(err, req, res);
 	});
}

function updateCardariDriverLocation(req, res) {
	if(svr_utils.checkParams(['driverId', 'lat', 'lng'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var userInfo = null;
	var userAdditional = null;
	var range = 1000;
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var version = req.params.version ? req.params.version : "";
	
	Async.waterfall([
	    function(next) {
	    	CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(driverInfo, next) {
	    	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
	    	
	    	driverInfo.locations = {
	    		type: "Point",
				coordinates: [ lng, lat ]
	    	};
	    	
	    	driverInfo.save(function(err) {
	    		driverInfo = driverInfo.getData();
	    		next(err);
			});
		}, function(next) {
			res.send({ result: error_code.SUCCESS });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function income_list(req, res) {
	if(svr_utils.checkParams(['driverId', 'date', 'dateType'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var dateType = parseInt(req.params.dateType);
	var searchStartTime;
	var searchEndTime;
	
	var dateStrArray = req.params.date.split("-");
	if(dateType == 0) {
		searchStartTime = new Date(dateStrArray[0] + "-" + dateStrArray[1] + "-" + dateStrArray[2] + " 00:00:00").getTime();
		searchEndTime = new Date(dateStrArray[0] + "-" + dateStrArray[1] + "-" + (parseInt(dateStrArray[2], 10) + 1) + " 00:00:00").getTime();
	} else {
		searchStartTime = new Date(dateStrArray[0] + "-" + dateStrArray[1] + "-01 00:00:00").getTime();
		searchEndTime = new Date(dateStrArray[0] + "-" + (parseInt(dateStrArray[1], 10) + 1) + "-01 00:00:00").getTime();
	}
	
	Async.waterfall([
		function(next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(driverInfo, next) {
	    	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
	    	
	    	_LogDriverBalance.find({driverId : req.params.driverId, createdTime: {'$gte' : searchStartTime, '$lt' : searchEndTime}}).sort({"createdTime" : -1}).exec(function(err, balanceList){
	    		next(err, driverInfo, balanceList);
	    	});
	    }, function(driverInfo, balanceList, next) {
	    	var retData = {
	    		totalCount : 0,
	    		totalIncome : 0,
	    		depositAmount : 0,
	    		balance : 0,
	    		completionTotalCount : 0,
	    		completionTotalAmount : 0,
	    		penaltyTotalCount : 0,
	    		penaltyTotalAmount : 0
	    	};
	    	
	    	var completionIds = [];
	    	if(balanceList && balanceList.length > 0) {
	    		retData.totalCount = balanceList.length;
	    		for(var i = 0; i < balanceList.length; i++) {
	    			var balanceData = balanceList[i];
	    			
	    			if(balanceData.balance > 0) {
	    				retData.totalIncome += balanceData.balance;
	    			}
	    			
	    			if(balanceData.logType == "callEnd") {
	    				retData.completionTotalCount++;
	    				retData.completionTotalAmount += balanceData.balance;
	    				
	    				if(dateType == 0) {
	    					completionIds.push(balanceData.callId);	
	    				}
	    			} else if(balanceData.logType == "callCancel") {
	    				retData.penaltyTotalCount++;
	    				retData.penaltyTotalAmount += balanceData.balance;
	    				
	    				if(dateType == 0) {
	    					completionIds.push(balanceData.callId);
	    				}
	    			}
	    		}
	    	}
	    	
	    	next(null, retData, completionIds);
	    }, function(retData, completionIds, next) {
	    	if(completionIds.length > 0) {
	    		_LogCall.find({"callId" : { "$in": completionIds }}).exec(function(_err, _callList) {
	    			next(_err, retData, _callList);
	    		});
	    	} else {
    			next(null, retData, []);
	    	}
	    }, function(retData, _callList, next) {
	    	var completionCallList = [];
	    	var penaltyCallList = [];
	    	if(_callList.length > 0) {
	    		for(var i = 0; i < _callList.length; i++) {
	    			var callData = _callList[i].getData();
	    			if(callData.status == "end" || callData.status == "done") {
	    				completionCallList.push({
	    					sequence : completionCallList.length + 1,
	    					startPointName : callData.start.address,
	    					endPointName : callData.end.address,
	    					elapsedTime : 0,
	    					charge : callData.money
	    				});
	    			} else if(callData.status == "cancel") {
	    				penaltyCallList.push({
	    					sequence : penaltyCallList.length + 1,
	    					startPointName : callData.start.address,
	    					endPointName : callData.end.address,
	    					elapsedTime : 0,
	    					charge : callData.money
	    				});
	    			}
	    		}
	    	}
	    	
	    	retData.completionList = completionCallList;
	    	retData.penaltyList = penaltyCallList;
	    	next(null, retData);
		}, function(retData, next) {
			retData.result = error_code.SUCCESS;
			
			res.send(retData);
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function deposit_and_withdraw_list(req, res) {
	if(svr_utils.checkParams(['driverId', 'from', 'to', 'page'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var limit = 20;
	var page = parseInt(req.params.page);
	var skip = page * limit;
	
	var fromDateStrArray = req.params.from.split("-");
	var toDateStrArray = req.params.to.split("-");
	searchStartTime = new Date(fromDateStrArray[0] + "-" + fromDateStrArray[1] + "-" + fromDateStrArray[2] + " 00:00:00").getTime();
	searchEndTime = new Date(toDateStrArray[0] + "-" + toDateStrArray[1] + "-" + toDateStrArray[2] + " 23:59:59").getTime();
	
	Async.waterfall([
		function(next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(driverInfo, next) {
	    	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
	    	
	    	_LogDriverBalance.find({driverId : req.params.driverId, createdTime: { '$gte' : searchStartTime, '$lte' : searchEndTime}}).sort({"createdTime" : -1}).skip(skip).limit(limit).exec(function(err, balanceList){
	    		next(err, driverInfo, balanceList);
	    	});
	    }, function(driverInfo, balanceList, next) {
	    	var retData = [];
	    	if(balanceList && balanceList.length > 0) {
	    		for(var i = 0; i < balanceList.length; i++) {
	    			var balanceData = balanceList[i];
	    			
	    			retData.push({
    	    			dateTime : balanceData.createdTime,
    	    			price : balanceData.balance,
    	    			balance : balanceData.afterBalance,
    	    			summary : balanceData.balance > 0 ? "입금" : "출금"
    		    	});
	    		}
	    	}
	    	
	    	res.send({ result: error_code.SUCCESS, depositAndWithdrawDataList : retData });
			next();
	    }
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function last_order_list(req, res) {
	if(svr_utils.checkParams(['driverId', 'from', 'to', 'page'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var limit = 20;
	var page = parseInt(req.params.page);
	var skip = page * limit;
	
	var fromDateStrArray = req.params.from.split("-");
	var toDateStrArray = req.params.to.split("-");
	searchStartTime = new Date(fromDateStrArray[0] + "-" + fromDateStrArray[1] + "-" + fromDateStrArray[2] + " 00:00:00").getTime();
	searchEndTime = new Date(toDateStrArray[0] + "-" + toDateStrArray[1] + "-" + toDateStrArray[2] + " 23:59:59").getTime();
	
	Async.waterfall([
		function(next) {
			CardariDriver.findById(new ObjectId(req.params.driverId)).exec(next);
	    }, function(driverInfo, next) {
	    	if(!driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
	    	
	    	_LogCall.find({"driver.driverId" : req.params.driverId, status: {'$in' : ['end', 'done']}, createdTime: { '$gte' : searchStartTime, '$lte' : searchEndTime}}).sort({"createdTime" : -1}).skip(skip).limit(limit).exec(function(err, callHistoryList){
	    		next(err, driverInfo, callHistoryList);
	    	});
	    }, function(driverInfo, callHistoryList, next) {
	    	var retData = [];
	    	if(callHistoryList && callHistoryList.length > 0) {
	    		for(var i = 0; i < callHistoryList.length; i++) {
	    			var callHistoryData = callHistoryList[i];
	    			
	    			retData.push({
    	    			dateTime : callHistoryData.createdTime,
    	    			payamount : callHistoryData.money,
    	    			startPointName : callHistoryData.start.address,
    	    			endPointName : callHistoryData.end.address,
    	    			summary : "일반",
    	    			centerName : "달빛기사",
    	    			centerPhoneNumber : "070-000-0000"
    		    	});
	    		}
	    	}
	    	
	    	res.send({ result: error_code.SUCCESS, lastOrderDataList : retData });
			next();
	    }
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function saveReqUnderWritingLog(req, res) {
	if(svr_utils.checkParams(['driverId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	CardariDriver.findById(new ObjectId(req.params.driverId)).exec(function(_err, _driverInfo) {
		if(_err) {
			errFunction(err, req, res);
		} else {
			if(!_driverInfo) {
				res.send({ result: error_code.NO_SUCH_DRIVER });
				return;
	    	}
			
			var driverInfo = _driverInfo.getData();
			
			var log = new _LogUnderWriting({
				driverId: driverInfo.driverId,
				phone: driverInfo.phone,
				phone1: driverInfo.phone1,
				uniqueId : driverInfo.uniqueId,
				name: driverInfo.name,
				socialNumber: driverInfo.socialNumber,
				licenseType: driverInfo.licenseType,
				licenseNumber: driverInfo.licenseNumber,
				email: driverInfo.email,
				status:	"request",
				message: "",
				apiDriverId: driverInfo.apiDriverId,
				logType: "request"
			});
			
			log.save(function(_err) {
				if(_err) {
					logger.error(_err.stack);
				}
				
				res.send({ result: error_code.SUCCESS });
			});
		}
	});
}

function saveReqDrivingLog(req, res) {
	if(svr_utils.checkParams(['callId'], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	var log = new _LogInsuranceDriving({
		callId:	req.params.callId,
		drivingMode: req.params.driving_mode,
		apiDrivingId: req.params.apiDrivingId,
		apiDriverId: req.params.api_driver_id,
		bizDrivingId: req.params.biz_driver_id,
		callType: req.params.call_type,
		driverCell:	req.params.driver_cell,
		orderMaker:	req.params.order_maker,
		orderMakerCall: req.params.order_maker_call,
		orderReceiver: req.params.order_receiver,
		orderReceiverCall: req.params.order_receiver_call,
		startAddress: req.params.start_address,
		startGps: req.params.start_gps,
		targetAddress: req.params.target_address,
		targetGps: req.params.target_gps,
		clientCell: req.params.client_cell,
		clientCarType: req.params.client_car_type,
		clientCarModel: req.params.client_car_model,
		drivingTime: req.params.driving_time,
		message: "",
		premiums: req.params.premiums,
	});
	
	Call.findById(new ObjectId(req.params.callId)).exec(function(err, callInfo) {
		if(err) {
			res.send({ result: error_code.NO_SUCH_CALL });
			return;
		}
		
		if (!callInfo) {
			res.send({ result: error_code.NO_SUCH_CALL });
			return;
		}
		
		callInfo.apiDrivingId = req.params.apiDrivingId;
		callInfo.premiums = req.params.premiums;
		
		callInfo.save(function(err) {
			if(err) {
				res.send({ result: error_code.NO_SUCH_CALL });
				return;
			}
			
			log.save(function(_err) {
				res.send({ result: error_code.SUCCESS });
			});
		});
	});
}

exports.initialize = initialize;
exports.registerCardariDriver = registerCardariDriver;
exports.editCardariDriver = editCardariDriver;
exports.tempRegisterCardariDriver = tempRegisterCardariDriver;
exports.updateCardariDriverConfirmData = updateCardariDriverConfirmData;
exports.checkCardariDriverConfirmData = checkCardariDriverConfirmData;
exports.cardariInsertInsuranceCompany = cardariInsertInsuranceCompany;
exports.cardariLogOut = cardariLogOut;
exports.checkCardariVersion = checkCardariVersion;
exports.updateCardariDriverLocation = updateCardariDriverLocation;
exports.income_list = income_list;
exports.deposit_and_withdraw_list = deposit_and_withdraw_list;
exports.last_order_list = last_order_list;
exports.loginCardariDriver = loginCardariDriver;
exports.checkCardaiRegister = checkCardaiRegister;
exports.saveReqUnderWritingLog = saveReqUnderWritingLog;
exports.saveReqDrivingLog = saveReqDrivingLog;
