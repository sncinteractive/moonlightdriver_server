var Async		= require('async');
var Bus			= DB.model('Bus');
var Beacon		= DB.model('ShuttleBeacon');

function initialize (server) {
	server.get('/beacon', beaconList);
	server.get('/beacon/list', getNearBeaconList);
	server.get('/beacon/isrealtime/:busId', getBeaconByBusId);
	server.get('/beacon/:beaconId', getBeaconByMac);
	server.post('/beacon/update/location', updateShuttleLocation);
}

/***
 * HTTP GET /bus
 * REQUEST PARAMETERS : len(한번에 가져올 길이), pageNo(페이지 수)
 * RESPONSE PARAMETERS : result(결과값), buses(버스 목록), length(총 버스 수)
 */
function beaconList (req, res) {

	Async.waterfall([
		function (next) {
			Beacon.find({}).exec(next);
		}, function (beacons, next) {
			for (var i in beacons) if (beacons.hasOwnProperty(i)) {
				beacons[i] = beacons[i].getData();
			}
			res.send({ result: error_code.SUCCESS, beacons: beacons});
			next();
			// Bus.find({}).sort({ _id: -1 }).skip((pageNo - 1) * len).limit(len).exec(function (err, list) {
			// 	next(err, buses, list);
			// });
		//}, function (buses, list, next) {
			
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getNearBeaconList(req, res) {
	if(svr_utils.checkParams(['lat', 'lng', 'range'], req.params)) {
		res.send({ result:  "Error"});
		//paramFunction(req, res);
		return;
	}

	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);

	Async.waterfall([

		function (next) {
		    Beacon.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				}
			], function(err, beacons) {
				console.log(beacons);
				next(err, beacons);
			});
		},function(beacons, next) {
			var retBeacons = [];
			var nowDate = new Date();
 			if(beacons) {
 				var listLength = beacons.length;
 				for(var i = 0; i < listLength; i++) {
 					var gap = nowDate.getTime() - new Date(beacons[i].lastTime).getTime();
					var min_gap = gap / 1000;
					
					if(min_gap < 60){
	 					retBeacons.push({
	 						beaconId:	beacons[i]._id,
	 						beaconMac:	beacons[i].beaconMac,
	 						busId:		beacons[i].busId,
	 						busNum:		beacons[i].busNum,
	 						text:		beacons[i].text,
	 						locations: 	beacons[i].locations,
	 					});
 					}
 				}
 			}
 			
 			res.send({ result: error_code.SUCCESS, beacon_list: retBeacons });
			next();
		}
	], function (err) {
		if (err) {
			console.log(err);
			errFunction(err, req, res);
		}
	});
}

		
function getBeaconByBusId(req,res){
	if (svr_utils.checkParams(['busId'], req.params)) {
		res.send({ result:  "Error"});
		//paramFunction(req, res);
        return;
	}
	Async.waterfall([
		function (next) {
			Beacon.find({busId : req.params.busId }).exec(next);
		}, function (beacons, next) {
			if(!beacons){
				res.send({result: error_code.NO_SUCH_BUS});
				return;
			}
			var retBeacons = [];
			var listLength = beacons.length;

			if(listLength == 0){
				res.send({result: error_code.NO_SUCH_BUS});
				return;
			}
			var nowDate = new Date();
			for(var i = 0; i < listLength; i++) {
				var gap = nowDate.getTime() - new Date(beacons[i].lastTime).getTime();
				var min_gap = gap / 1000;
				
				if(min_gap < 60){
					retBeacons.push({
						beaconId:	beacons[i]._id,
						beaconMac:	beacons[i].beaconMac,
						busId:		beacons[i].busId,
						busNum:		beacons[i].busNum,
						text:		beacons[i].text,
						locations: 	beacons[i].locations,
					});
				}
			}


			res.send({ result: error_code.SUCCESS, beacon_list: retBeacons });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}


function getBeaconByMac (req, res) {

	if (svr_utils.checkParams(['beaconId'], req.params)) {
		res.send({ result:  "Error"});
		//paramFunction(req, res);
        return;
	}

	Async.waterfall([
		function (next) {
			Beacon.findOne({beaconMac : req.params.beaconId }).exec(next);
		}, function (beacon, next) {
			
			if (!beacon) {
				res.send({ result:  "요청하신 비콘을 찾을 수 없습니다."});
				return;
			}
			console.log(beacon);
			res.send({ result: error_code.SUCCESS, beacon: beacon});
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}


function updateShuttleLocation(req, res) {
	if(svr_utils.checkParams(['beaconId', 'lat', 'lng'], req.params)) {
		res.send({ result:  "Error"});
		//paramFunction(req, res);
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
	    function(next) {
	    	Beacon.findOne({beaconMac: req.params.beaconId}).exec(next);
	    }, function(_beaconInfo, next) {
	    	if(!_beaconInfo) {
				res.send({ result: "요청하신 비콘을 찾을 수 없습니다."});
				return;
	    	}
	    	_beaconInfo.lastTime = new Date();

	    	_beaconInfo.locations = {
	    		type: "Point",
				coordinates: [ lng, lat ]
	    	};	
	    	
	    	_beaconInfo.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: error_code.SUCCESS });
					next();
				}
			});
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
			res.send({ result: err });
		}
	});
}

exports.initialize = initialize;
exports.beaconList = beaconList;
exports.getBeaconByMac = getBeaconByMac;
exports.getBeaconByBusId = getBeaconByBusId;
exports.updateShuttleLocation = updateShuttleLocation;
exports.getNearBeaconList = getNearBeaconList;