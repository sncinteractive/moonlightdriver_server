var Async		= require('async');
var Notice 		= DB.model('Notice');
var UserNotice	= DB.model('UserNotice');
var _LogNotice	= DB.model('_LogNotice');
var Counter		= DB.model('Counter');

function initialize (server) {
	server.post('/notice/create', createNotice);
	server.get('/notice/list', getNoticeList);
	server.get('/notice/user/list', getUserNoticeList);
}

function createNotice(req, res) {
	if (svr_utils.checkParams(["title", "contents", "image", "link", "startDate", "endDate", "createdDate"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
 		function (next) {
			Counter.getNextSequence('notice_seq', function(err, _counter) {
                if(err) {
                    next(err);
                } else {
					var notice = new Notice({
						seq: _counter.seq,
						title: req.params.title,
						contents: req.params.contents,
						image: req.params.image,
						link: req.params.link,
						startDate: req.params.startDate,
						endDate: req.params.endDate,
						createdDate: new Date().getTime(),
						updatedDate: new Date().getTime()
					});

					notice.save(function (err) {
						if(err) {
							next(err, next);
						} else {
							res.send({ result: error_code.SUCCESS });
							next(null, notice);
						}
					});
				}
			});
 		},
 		function(notice, next) {
 			var notice_log = new _LogNotice({
				seq: notice.seq,
 				title:	notice.title,
 				contents: notice.contents,
 				image: notice.image,
 				link:	notice.link,
 				startDate: notice.startDate,
 				endDate: notice.endDate,
 				createdDate: notice.createdDate,
				updatedDate: notice.updatedDate
 			});
 			
 			notice_log.save();
 			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getNoticeList(req, res) {
	Async.waterfall([
 		function (next) {
 			var nowTime = new Date().getTime();
 			Notice.find({startDate : {'$lte' : nowTime}, endDate : { '$gt' : nowTime}}).sort({ seq: -1 }).exec(next);
 		},
 		function(notice, next) {
 			var retNotice = [];
 			if(notice) {
 				var listLength = notice.length;
 				for(var i = 0; i < listLength; i++) {
 					retNotice.push(notice[i].getData());
 				}
 			}
 			
 			res.send({ result: error_code.SUCCESS, notices: retNotice });
			next();
 		}
     ], function (err) {
     	if (err) errFunction(err, req, res);
     });
}

function getUserNoticeList(req, res) {
	Async.waterfall([
  		function (next) {
  			var nowTime = new Date().getTime();
  			UserNotice.find({startDate : {'$lte' : nowTime}, endDate : { '$gt' : nowTime}}).sort({ seq: -1 }).exec(next);
  		},
  		function(notice, next) {
  			var retNotice = [];
  			if(notice) {
  				var listLength = notice.length;
  				for(var i = 0; i < listLength; i++) {
  					retNotice.push(notice[i].getData());
  				}
  			}
  			
  			res.send({ result: error_code.SUCCESS, notices: retNotice });
 			next();
  		}
  	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

exports.initialize = initialize;
exports.createNotice = createNotice;
exports.getNoticeList = getNoticeList;
exports.getUserNoticeList = getUserNoticeList;

