var Async	= require('async');
var fs 		= require('fs');
var mv 		= require('mv');

function initialize (server) {
    server.post('/file', uploadFile);
    server.get('/file/:fileTime/:fileName', sendFile);
}

function uploadFile (req, res) {
    var extList = [ "png", "jpg", "jpeg", "gif" ];
    var result = false;
    
    for (var i in extList) {
    	if (extList.hasOwnProperty(i)) {
            if (String(req.files.file).indexOf(extList[i]) != -1) {
            	result = true;
            }
    	}
    }
    
	var time = new Date().getTime();
	var fileorignname = req.files.fileToUpload.name;
    var fileName = decodeURI( time + "_" + fileorignname );
    
    mv(req.files.fileToUpload.path, rootPath + "/images/" + fileName, { mkdirp: true }, function (err) {
        if (err) {
        	res.send({ result: error_code.INTERNAL_ERROR , log: err });
        } else {
        	res.send({ result: error_code.SUCCESS, time: time, file: req.files.fileToUpload.name });
        }
    });
}

function sendFile (req, res) {
    var fileName = req.params.fileTime + "_" + req.params.fileName;
    fs.readFile(rootPath + "/images/" + fileName, function (err, file) {
        if (err) {
            res.send({ result: error_code.INTERNAL_ERROR , log: err });
            res.writeHead(500);
        } else {
            res.writeHead(200);
            res.end(file, 'binary');
        }
    });
}

exports.initialize = initialize;
exports.uploadFile = uploadFile;
exports.sendFile = sendFile;
