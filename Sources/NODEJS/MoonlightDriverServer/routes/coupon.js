var Async			= require('async');
var User      		= DB.model('User');
var Coupon			= DB.model('Coupon');
var UserCoupon		= DB.model('UserCoupon');
var ObjectId		= DB.Types.ObjectId;

function initialize (server) {
	server.post('/settings/get/coupons', couponList);
	server.post('/settings/get/coupon/detail', couponDetail);
	server.post('/settings/update/coupon/status', updateCouponStatus);
}

function couponList(req, res) {
	if(svr_utils.checkParams(["userId"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	Async.waterfall([
	    function(next) {
	    	UserCoupon.find({userId: req.params.userId, delYn: 'N'}).exec(next);
	    }, function(_userCouponList, next) {
	    	var userCouponListCnt = _userCouponList.length;
	    	
	    	if(userCouponListCnt > 0) {
	    		var couponIdList = [];
	    		for(var i = 0; i < userCouponListCnt; i++) {
	    			couponIdList.push(new ObjectId(_userCouponList[i].couponId));
	    		}
	    		
	    		Coupon.find({"_id" : { "$in": couponIdList }}).exec(function(_err, _couponList) {
	    			next(_err, _userCouponList, _couponList);
	    		});
	    	} else {
	    		next(null, null, null);
	    	}
		}, function(_userCouponList, _couponList, next) {
			if(!_userCouponList || _userCouponList.length <= 0) {
				res.send({ result: error_code.SUCCESS, couponList: [] });
				return;
			}
			
			var retUserCouponList = [];
			for(var i = 0; i < _userCouponList.length; i++) {
				var userCouponInfo = _userCouponList[i].getData();
				
				for(var j = 0; j < _couponList.length; j++) {
					var couponInfo = _couponList[j].getData();
					
					if(userCouponInfo.couponId == couponInfo.couponId) {
						userCouponInfo.couponName = couponInfo.couponName;
						userCouponInfo.couponDesc = couponInfo.couponDesc;
						userCouponInfo.couponImgPath = couponInfo.couponImgPath;
						userCouponInfo.couponOrgImgPath = couponInfo.couponOrgImgPath;
						userCouponInfo.couponStartDate = couponInfo.couponStartDate;
						userCouponInfo.couponEndDate = couponInfo.couponEndDate;
												
						retUserCouponList.push(userCouponInfo);
						
						break;
					}
				}
			}
			
			res.send({ result: error_code.SUCCESS, couponList: retUserCouponList });
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function couponDetail(req, res) {
	if(svr_utils.checkParams(["userId", "userCouponId", "couponId"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	UserCoupon.findOne({_id: new ObjectId(req.params.userCouponId), userId: req.params.userId}).exec(function (err, _userCouponInfo) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(!_userCouponInfo) {
				res.send({ result: error_code.NO_SUCH_COUPON });
				return;
			}
			
			var retUserCouponInfo = _userCouponInfo.getData();
			
			Coupon.findOne({_id : new ObjectId(req.params.couponId)}).exec(function(_err, _couponInfo) {
    			if(_err) {
    				errFunction(_err, req, res);
    			} else {
    				if(!_couponInfo) {
    					res.send({ result: error_code.NO_SUCH_COUPON });
    					return;
    				}
    				
    				retUserCouponInfo.couponName = _couponInfo.couponName;
    				retUserCouponInfo.couponDesc = _couponInfo.couponDesc;
    				retUserCouponInfo.couponImgPath = _couponInfo.couponImgPath;
    				retUserCouponInfo.couponOrgImgPath = _couponInfo.couponOrgImgPath;
    				retUserCouponInfo.couponStartDate = _couponInfo.couponStartDate;
    				retUserCouponInfo.couponEndDate = _couponInfo.couponEndDate;
    				
    				res.send({ result: error_code.SUCCESS, couponDetail: retUserCouponInfo });
    			}
    		});
		}
	});
}

function updateCouponStatus(req, res) {
	if(svr_utils.checkParams(["userId", "userCouponId", "status"], req.params)) {
		res.send({ result: error_code.NOT_ENOUGH_PARAMS });
		return;
	}
	
	UserCoupon.findOneAndUpdate({ _id: new ObjectId(req.params.userCouponId), userId: req.params.userId }, { "$set": { status : req.params.status }}, { new: true }).exec(function (err, _userCouponInfo) {
		if(err) {
			errFunction(err, req, res);
		} else {
			res.send({ result: error_code.SUCCESS });
		}
	});
}

exports.initialize = initialize;
exports.couponList = couponList;
exports.couponDetail = couponDetail;
exports.updateCouponStatus = updateCouponStatus;
