var net = require('net');

function Connection(port, host, handler, options) {
//	logger.info('===== connection_manager ===== constructor!');
	
	if (!port || !host || !handler) {
		throw "Port, host, and handler are required to connect.";
	}
	
	this.port    = port;
	this.host    = host;
	this.handler = handler;
	this.backoff = 100;
	this.retry   = true;
	this.count   = 0;
	
	this.createSocket(options);
	this.connect();
}

Connection.prototype.reconnect = function() {
//	logger.info('===== connection_manager ===== reconnect()!');

	var that = this;
	
	setTimeout(function () {
		that.connect();
	}, this.backoff);
};

Connection.prototype.connect = function() {
//	logger.info('===== connection_manager ===== connect()!');
	if (!this.retry) {
		return;
	}
	
	this.sock.connect(this.port, this.host);
};

Connection.prototype.createSocket = function (options) {
	var sock = new net.Socket(options);
	var that = this;
	
	sock.disconnect = function() {
//		logger.info('===== connection_manager ===== event disconnect()!');
		that.handler.onReset();
		that.reconnect();
	};
	
	sock.on('close', function() {
//		logger.info('===== connection_manager ===== event close()!');
		
		if(!that.handler) {
			return;
		}
		
		that.handler.onReset();
		that.reconnect();
	});
	
	sock.on('error', function(_err) {
//		logger.info('===== connection_manager ===== event error()!');
		
		if(!that.handler) {
			return;
		}
		
		that.handler.onError(_err);
	});
	
	sock.on('data', function(_buff) {
//		logger.info('===== connection_manager ===== event data()! - ' + _buff);
		
		if(!that.handler) {
			return;
		}
		
		that.handler.onData(_buff);
	});
	
	sock.on('connect', function() {
//		logger.info('===== connection_manager ===== event connect()!');
		that.backoff = 100; // Reset backoff on success
		
		if(!that.handler) {
			return;
		}
		
		if (that.count) {
			that.handler.onReconnect();
		} else {
			that.handler.onConnect(sock);
		}
		
		that.count++;
	});
	
	this.sock = sock;
};

module.exports = (function() {
	var conns = {};
	
	var me = {
		connect: function(port, host, handler, options) {
			var conn = new Connection(port, host, handler, options);
			conns[host + ':' + port] = conn;
		},
		getConnection: function(port, host) {
			return conns[host + ':' + port];
		},
		disconnectAll: function() {
			var key;
			
			for(key in conns) {
				if(conns.hasOwnProperty(key)) {
					conns[key].sock.disconnect();
				}
			}
		}
	};
	
	return me;
}());

process.on("uncaughtException", function (err) {
	logger.error("===== connection_manager ===== uncaughtException: " + err.stack);
});
