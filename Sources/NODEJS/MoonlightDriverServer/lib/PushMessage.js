var gcm	= require("node-gcm");
var apns = require('apns');

function PushMessage() {
	this.sender = new gcm.Sender(config.GCM.SENDER_ID);
	this.delayWhileIdle = true;
	this.timeToLive = 3;
};

PushMessage.prototype.createMessage = function (_message_data) {
	var message = new gcm.Message({
		delayWhileIdle: this.delayWhileIdle,
		timeToLive: this.timeToLive,
		data: _message_data
	});
	
	return message;
};

PushMessage.prototype.sendMessage = function (_message_data, _gcm_list, _callback) {
	var message = this.createMessage(_message_data);
		
	this.sender.send(message, _gcm_list, function (err, result) {
		_callback(err);
	});
};

PushMessage.prototype.sendIOSMessage = function (_alertMsg, _payload, _token) {
	var options = {
//		gateway : "gateway.sandbox.push.apple.com",
		certFile: rootPath + "/cert/cert.pem",
		keyFile: rootPath + "/cert/key.pem",
		debug: true
	};
		
	var apnsConnection = new apns.Connection(options);
	
	var noti = new apns.Notification();

    noti.badge = 1;
    noti.alert = _alertMsg;
    noti.payload = _payload;
    noti.device = new apns.Device(_token);

    apnsConnection.sendNotification(noti);
    
    logger.error("sendIOSMessage() : " + JSON.stringify(_payload));
};

module.exports = PushMessage;
