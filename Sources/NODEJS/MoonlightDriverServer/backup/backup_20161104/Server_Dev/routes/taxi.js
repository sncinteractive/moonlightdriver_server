var Async		= require('async');
var TaxiCarpool = DB.model('TaxiCarpool');
var TaxiFind	= DB.model('TaxiFind');
var Driver      = DB.model('Driver');
var Location    = DB.model('Location');
var _LogTaxi	= DB.model('_LogTaxi');
var _LogTaxiFind = DB.model('_LogTaxiFind');
var gcm			= require("node-gcm");
var ObjectId	= DB.Types.ObjectId;
var sender;

function initialize (server) {
	sender = new gcm.Sender("AIzaSyDb5DTLNl-uMS820Bozj5S1AVohg1g_uHY");
	server.post('/taxi/list', taxiList);
	server.get('/taxi/list/registrant', taxiRegistrantList);
	server.post('/taxi/:taxiId/get', getTaxi);
	server.post('/taxi/:taxiId/register', registerTaxi);
	server.post('/taxi', createTaxi);
	server.post('/taxi/edit', editTaxi);
	server.post('/taxi/delete', deleteTaxi);
	server.patch('/taxi/:taxiId', patchTaxi);
	server.post('/taxi/:taxiId/accept', acceptTaxi);
	server.post('/taxi/:taxiId/decline', declineTaxi);
	server.post('/driver/update', updateStatus);
	server.post('/driver/find', findStatus);
}

function taxiRegistrantList(req, res) {
	var start_time = new Date().getTime();
	
	if (checkParams(['lat', 'lng', 'range'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	var range = parseFloat(req.params.range);
	
	Async.waterfall([
		function (next) {
		    TaxiCarpool.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { 
						"time" : { "$gte" : new Date().getTime() }
					}
				}
			], function(err, taxiCarpool) {
				next(err, taxiCarpool);
			});
		}, function (taxiCarpool, next) {
			var registrantList = [];
			var taxiCarpoolList = [];
			var listLength = taxiCarpool.length;
			if(listLength > 0) {
				for(var i = 0; i < listLength; i++) {
					var taxi = {
						taxiId : taxiCarpool[i]._id,
						driverId : taxiCarpool[i].driverId,
						registrant: taxiCarpool[i].registrant
					};
					
					taxiCarpoolList.push(taxi);
					registrantList.push(taxi.driverId);
				}
				
				TaxiFind.find({"driverId" : {"$in": registrantList}}).exec(function(err, driverPoints) {
					if(err) {
						next(err);
					} else {
						var retDriverPoints = {};
						var driverPointsLength = driverPoints.length;
						for(var i = 0; i < driverPointsLength; i++) {
							var driverPointInfo = driverPoints[i].getListData();
							retDriverPoints[driverPointInfo.driverId] = driverPointInfo.location;
						}
						
						for(var i = 0; i < listLength; i++) {
							if(retDriverPoints[taxiCarpoolList[i].driverId]) {
								taxiCarpoolList[i].location = retDriverPoints[taxiCarpoolList[i].driverId];
							}
						}

						next(null, taxiCarpoolList);
					}
				});
			} else {
				next(null, []);
			}
		}, function(_registrant_list, next) {
			var limitTime = new Date().getTime() - (60 * 60 * 1000);
			
		    TaxiFind.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
//				},
//				{
//				TODO : 앱 1.2.1 버전 업데이트 시 주석 제거
//					"$match": {
//						"date" : { "$gt" : limitTime }
//						"ynLogin" : "Y"
//					}
				}
			], function(err, finds) {
				next(err, finds, _registrant_list);
			});
		}, function (finds, _registrant_list, next) {
			var retArray = [];
			for(var i = 0; i < finds.length; i++) {
				var retFind = {
					driverId: finds[i].driverId,
					phone:	finds[i].phone,
					date:	finds[i].date,
					gcmId: "",
					location: {
						type: finds[i].location.type,
						coordinates: finds[i].location.coordinates
					}
				};
				
				retArray.push(retFind);
			}
			
			var end_time = new Date().getTime();
			logger.info((end_time - start_time) + "ms [" + req.getPath() + "]");
			
			res.send({result: main.result.SUCCESS, taxiCarpoolRegistrant: _registrant_list, driverList: retArray });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function patchTaxi (req, res) {
	if (checkParams(["taxiId"], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function (taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			if (req.params.hasOwnProperty("start_text")) {
				taxiCarpool.start = {
					text: req.params.start_text,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.start_lng), parseFloat(req.params.start_lat)]
					} 
				};
			}
			
			if (req.params.hasOwnProperty("end_text")) {
				taxiCarpool.end = {
					text: req.params.stop_text,
					location: {
						type: "Point",
						coordinates: [parseFloat(req.params.stop_lng), parseFloat(req.params.stop_lat)]
					}
				};
			} 

			if (req.params.hasOwnProperty("money")) taxiCarpool.money = parseInt(req.params.money);
			if (req.params.hasOwnProperty("max")) taxiCarpool.drivers.max = parseInt(req.params.max);
			if (req.params.hasOwnProperty("time")) taxiCarpool.time = parseInt(req.params.time);
			if (req.params.hasOwnProperty("divide")) taxiCarpool.divideN = (req.params.divide == 1);
			
			taxiCarpool.save(function (err) {
				if (err){
					next(err);
				} else {
					res.send({ result: main.result.SUCCESS, taxi: taxiCarpool.getData() });
					next();
				}
			});
		}
    ], function (err) {
    	if (err) errFunction(err, req, res);
    });
}

// Taxi List 가져오는 API이다.
// API : HTTP POST /taxi/list
function taxiList (req, res) {
	if (checkParams(['lat', 'lng', 'dist'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var pageNum = req.params.pageNum ? parseInt(req.params.pageNum) : 0;
	var limit = req.params.limit ? parseInt(req.params.limit) : 20;
	
	pageNum = pageNum * limit;
	
	Async.waterfall([
		function (next) {
			var lat = parseFloat(req.params.lat);
		    var lng = parseFloat(req.params.lng);
		    var range = parseFloat(req.params.dist);
		    
		    TaxiCarpool.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
				},
				{ 
					"$match": { 
						"time" : { "$gte" : new Date().getTime() }
					}
				},
				{ 
					"$limit" : limit 
				},
				{ 
					"$skip" : pageNum 
				}
			], function(err, taxiCarpool) {
				next(err, taxiCarpool);
			});
		}, function (taxiCarpool, next) {
			var tmpTaxis = [];
			var registrantList = [];
			for(var i = 0; i < taxiCarpool.length; i++) {
				var taxi = {
					taxiId : taxiCarpool[i]._id,
					driverId: taxiCarpool[i].driverId,
					registrant: taxiCarpool[i].registrant,
					isValid : taxiCarpool[i].isValid,
					isCatch : taxiCarpool[i].isCatch,
					isPush : taxiCarpool[i].isPush,
					money : taxiCarpool[i].money,
					host : taxiCarpool[i].host,
					time : taxiCarpool[i].time,
					divideN : taxiCarpool[i].divideN,
					drivers : {
						max : taxiCarpool[i].drivers.max,
						list : taxiCarpool[i].drivers.list
					},
					end : {
						text : taxiCarpool[i].end.text,
						location : {
							type : taxiCarpool[i].end.location.type,
							coordinates : taxiCarpool[i].end.location.coordinates
						}
					},
					start : {
						text : taxiCarpool[i].start.text,
						location : {
							type : taxiCarpool[i].start.location.type,
							coordinates : taxiCarpool[i].start.location.coordinates
						}
					},
					distance : taxiCarpool[i].distance
				};
				
				tmpTaxis.push(taxi);
				registrantList.push(taxi.driverId);
			}
			
			if(registrantList.length > 0) {
				TaxiFind.find({"driverId" : {"$in": registrantList}}).exec(function(err, driverPoints) {
					if(err) {
						next(err);
					} else {
						var retDriverPoints = [];
						var driverPointsLength = driverPoints.length;
						for(var i = 0; i < driverPointsLength; i++) {
							var driverPointInfo = driverPoints[i].getData();
							retDriverPoints.push(driverPointInfo);
						}

						next(null, retDriverPoints, tmpTaxis);
					}
				});
			} else {
				next(null, [], tmpTaxis);
			}
		}, function(_driver_point, _taxi_list, next) {
			var lat = parseFloat(req.params.lat);
		    var lng = parseFloat(req.params.lng);
		    var range = parseFloat(req.params.dist);
		    var limitTime = new Date().getTime() - (60 * 60 * 1000);
		    
		    TaxiFind.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
//				},
//				{
//				TODO : 앱 1.2.1 버전 업데이트 시 주석 제거
//					"$match": {
//                        "date" : { "$gt" : limitTime }
//						"ynLogin" : "Y"
//					}
				}
			], function(err, finds) {
				next(err, finds, _taxi_list, _driver_point);
			});
		}, function (finds, _taxi_list, _driver_point, next) {
			var retArray = [];
			for(var i = 0; i < finds.length; i++) {
				var retFind = {
					driverId: finds[i].driverId,
					phone:	finds[i].phone,
					date:	finds[i].date,
					gcmId: "",
					location: {
						type: finds[i].location.type,
						coordinates: finds[i].location.coordinates
					}
				};
				
				retArray.push(retFind);
			}
			
			res.send({result: main.result.SUCCESS, list: _taxi_list, driverPoints : _driver_point, driverList: retArray });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// Taxi List 가져오는 API이다.
// API : HTTP POST /taxi/list
function getTaxi (req, res) {
	if (checkParams(['taxiId'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function (taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			var nowTime = new Date().getTime();
			if(taxiCarpool.time < nowTime) {
				res.send({ result: main.result.EXPIRED_TAXI });
				return;
			}
			
			taxiCarpool = taxiCarpool.getData();
			
			var driverCount = taxiCarpool.drivers.list.length;
			var driverPhones = [];
			for(var i = 0; i < driverCount; i++) {
				var driverInfo = taxiCarpool.drivers.list[i];
				if(driverInfo.status == "accept" && driverInfo.isAccept) {
					driverPhones.push(driverInfo.phone);
				}
			}
			
			if(driverPhones.length > 0) {
				TaxiFind.find({"phone" : {"$in": driverPhones}}).exec(function(err, driverPoints) {
					if(err) {
						next(err);
					} else {
						var retDriverPoints = [];
						var driverPointsLength = driverPoints.length;
						for(var i = 0; i < driverPointsLength; i++) {
							var driverPointInfo = driverPoints[i].getData();
							retDriverPoints.push(driverPointInfo);
						}

						res.send({result: main.result.SUCCESS, taxi: taxiCarpool, driverPoints : retDriverPoints });
						next();
					}
				});
			} else {
				res.send({result: main.result.SUCCESS, taxi: taxiCarpool, driverPoints : [] });
				next();
			}
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

//Taxi List 가져오는 API이다.
//API : HTTP POST /taxi/list
function deleteTaxi (req, res) {
	if (checkParams(['taxiId', 'driverId'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function (taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}

			Driver.findById(new ObjectId(req.params.driverId)).exec(function (err, hostInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, next) {
			if (!hostInfo) {
				res.send({ result: main.result.NO_SUCH_TAXI_HOST });
				return;
			}

			if(taxiCarpool.host != hostInfo.phone) {
				res.send({ result: main.result.NOT_MATCH_HOST });
				return;
			}

			TaxiCarpool.remove({"_id" : new ObjectId(req.params.taxiId)}, function(err, data) {
				if(err) {
					next(err);
				} else {
					res.send({ result: main.result.SUCCESS });
					next();
				}
			});
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

// [게스트만이 가능함.] 택시 카풀에 동승을 요청한다.
// API : HTTP POST /taxi/:taxiId/register
function registerTaxi (req, res) {
	if (checkParams(['taxiId' ,'phone','message'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			// Taxi 검색!
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function (taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			Driver.findOne({ phone: taxiCarpool.host }).exec(function (err, hostInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, next) {
			if (!hostInfo) {
				res.send({ result: main.result.NO_SUCH_TAXI_HOST });
				return;
			}
			
			Driver.findOne({ phone: req.params.phone }).exec(function (err, guestInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo, guestInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			if (!guestInfo) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			if(hostInfo.phone == guestInfo.phone) {
				res.send({ result: main.result.NOT_ALLOW_HOST });
				return;
			}
			
			var nowTime = new Date().getTime();
			if(taxiCarpool.time < nowTime) {
				res.send({ result: main.result.EXPIRED_TAXI });
				return;
			}
			
			var isExist = false;
			for(i =0 ; i< taxiCarpool.drivers.list.length; i++){
				if(taxiCarpool.drivers.list[i].phone == guestInfo.phone){
					if(taxiCarpool.drivers.list[i].status == "wait" || taxiCarpool.drivers.list[i].status == "accept") {
						res.send({ result: main.result.DUPLICATE_TAXI });
						return;						
					} else {
						isExist = true;
						taxiCarpool.drivers.list[i].isAccept = false;
						taxiCarpool.drivers.list[i].status = "wait";
					}
				}
			}
			
			if(!isExist) {
				taxiCarpool.drivers.list.push({
					phone:      req.params.phone,
					isAccept:   false,
					status: 	"wait"
				});				
			}
			
			taxiCarpool.save();
			
			next(null, taxiCarpool, hostInfo, guestInfo);
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			var acceptDriverCount = 0;
			for(i =0 ; i< taxiCarpool.drivers.list.length; i++){
				if(taxiCarpool.drivers.list[i].status == "accept"){
					acceptDriverCount++;
				}
			}
			
			if(acceptDriverCount > taxiCarpool.drivers.max){
				res.send({ result: main.result.MAX_TAXI });
				return;
			}
			
			var message = new gcm.Message({
				delayWhileIdle: true,
				timeToLive: 3,
				data: {
					type: 'request_taxi',
					taxiId: req.params.taxiId,
					phone: req.params.phone,
					message: req.params.message
				}
			});
			
			sender.send(message, [hostInfo.gcmId], function (err, result) {
				next(err, taxiCarpool, hostInfo, guestInfo);
			});
		}, function (_taxiCarpool, _hostInfo, _guestInfo, next) {
			var hostDriverInfo = _hostInfo.getData();
			var guestDriverInfo = _guestInfo.getData();
			var taxiCarpoolInfo = _taxiCarpool.getData();
			
			res.send({ result: main.result.SUCCESS, driver: hostDriverInfo });
			
			var log_taxi = new _LogTaxi({
				driverId:	hostDriverInfo.driverId,
				taxiId:		taxiCarpoolInfo.taxiId,
				isValid:	taxiCarpoolInfo.isValid,
				isPush:		taxiCarpoolInfo.isPush,
				isCatch:	taxiCarpoolInfo.isCatch,
				start: {
					text: taxiCarpoolInfo.start.text,
					location: {
						type: taxiCarpoolInfo.start.location.type,
						coordinates: taxiCarpoolInfo.start.location.coordinates
					}
				},
				end: {
					text: taxiCarpoolInfo.end.text,
					location: {
						type: taxiCarpoolInfo.end.location.type,
						coordinates: taxiCarpoolInfo.end.location.coordinates
					}
				},
				money:		taxiCarpoolInfo.money,
				host:		taxiCarpoolInfo.host,
				drivers: {
					max: taxiCarpoolInfo.drivers.max,
					list: taxiCarpoolInfo.drivers.list
				},
				time:		taxiCarpoolInfo.time,
				divideN: 	taxiCarpoolInfo.divideN,
				req_driverId:	guestDriverInfo.driverId,
				message:	req.params.message,
				logType:	"req_share",
				logDate:	new Date()
			});
			
			log_taxi.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}


// 새 카풀 데이터를 추가한다.
// API : HTTP POST /taxi
function createTaxi (req, res) {
	var essentialParams = ['phone', 'time', 'start_lat', 'stop_lat', 'start_lng', 'stop_lng', 'start_text', 'stop_text', 'max', 'time', 'money', 'divide', 'registrant'];
	if (checkParams(essentialParams, req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (_driver, next) {
			if(!_driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			var taxiCarpool = new TaxiCarpool({
				driverId:	req.params.driverId,
				registrant: req.params.registrant,
				isValid: true,
				isCatch: req.params.isCatch,
				isPush: req.params.isPush,
				money: parseInt(req.params.money),
				start: {
					text: req.params.start_text,
					location : {
						type: "Point",
						coordinates: [parseFloat(req.params.start_lng), parseFloat(req.params.start_lat)]
					}
				},
				end: {
					text: req.params.stop_text,
					location : {
						type: "Point",
						coordinates: [parseFloat(req.params.stop_lng), parseFloat(req.params.stop_lat)]
					}
				},
				host: req.params.phone,
				drivers: {
					max: parseInt(req.params.max),
					list: [{
						phone:      req.params.phone,
						isAccept:   true,
						status: 	"accept"
					}]
				},
				time: parseInt(req.params.time),
				divideN: (req.params.divide == 1)
			});
			
			taxiCarpool.save(function (err) {
				next(err, taxiCarpool, _driver);
			});			
		}, function (taxiCarpool, _driver, next) {
			var driverInfo = _driver.getData();
			var taxiCarpoolInfo = taxiCarpool.getData();
			res.send({ result: main.result.SUCCESS, taxi: taxiCarpoolInfo });
			
			next(null, taxiCarpoolInfo, driverInfo);
		}, function(taxiCarpoolInfo, driverInfo, next){
			if(req.params.isPush == 1) {
				var lat = parseFloat(req.params.start_lat);
			    var lng = parseFloat(req.params.start_lng);
			    var limitTime = new Date().getTime() - (60 * 60 * 1000);
			    
			    TaxiFind.aggregate([
					{
						"$geoNear": {
							"near": {
								"type": "Point",
								"coordinates": [lng, lat]
							},
							"distanceField": "distance",
							"maxDistance": 5000,
							"spherical": true,
							"limit": 100000
						}
					},
					{
						"$match": {
							"date" : { "$gt" : limitTime }
						}
					}
				], function(err, finds) {
					next(err, taxiCarpoolInfo, driverInfo, finds);
				});				
			} else {
				next(null, taxiCarpoolInfo, driverInfo, null);
			}
		}, function(taxiCarpoolInfo, driverInfo, finds, next) {
			if(req.params.isPush == 1) {
				var nickName = req.params.phone.substr(req.params.phone.length - 4, 4);;
				var message = new gcm.Message({
	                delayWhileIdle: true,
	                timeToLive: 3,
	                data: {
	                    type: 'create_taxi',
	                    taxiId: taxiCarpoolInfo.taxiId,
	                    phone: req.params.phone,
	                    message: nickName + "기사님이 새로운 택시카풀신청을 등록하셨습니다."
	                }
	            });
				
				var retLength = finds.length;
				if(retLength > 0) {
					var retGcmId = [];
					for(var i = 0; i < retLength; i++) {
						if(finds[i].driverId != req.params.driverId) {
							retGcmId.push(finds[i].gcmId);							
						}
					}
					
					if(retGcmId.length > 0) {
//						logger.info("retGcmId : ", retGcmId);
						
						sender.send(message, retGcmId, function (err, result){
							if(err) {
								errFunction(err, req, res);
							}
//							logger.info("send gcm\n");
						});
					}
				}
			}
			
			next(null, taxiCarpoolInfo, driverInfo);
		}, function(taxiCarpoolInfo, driverInfo, next) {
			var log_taxi = new _LogTaxi({
				driverId:	driverInfo.driverId,
				taxiId:		taxiCarpoolInfo.taxiId,
				isValid:	taxiCarpoolInfo.isValid,
				isPush:		taxiCarpoolInfo.isPush,
				isCatch:	taxiCarpoolInfo.isCatch,
				start: {
					text: taxiCarpoolInfo.start.text,
					location: {
						type: taxiCarpoolInfo.start.location.type,
						coordinates: taxiCarpoolInfo.start.location.coordinates
					}
				},
				end: {
					text: taxiCarpoolInfo.end.text,
					location: {
						type: taxiCarpoolInfo.end.location.type,
						coordinates: taxiCarpoolInfo.end.location.coordinates
					}
				},
				money:		taxiCarpoolInfo.money,
				host:		taxiCarpoolInfo.host,
				drivers: {
					max: taxiCarpoolInfo.drivers.max,
					list: taxiCarpoolInfo.drivers.list,
				},
				time:		taxiCarpoolInfo.time,
				divideN: 	taxiCarpoolInfo.divideN,
				req_driverId:	"",
				message:	"",
				logType:	"create_share",
				logDate:	new Date()
			});
			
			log_taxi.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function editTaxi (req, res) {
	var essentialParams = ['taxiId', 'phone', 'time', 'start_lat', 'stop_lat', 'start_lng', 'stop_lng', 'start_text', 'stop_text', 'max', 'time', 'money', 'divide', 'registrant'];
	if (checkParams(essentialParams, req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function(_driver, next) {
			if(!_driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(function (err, taxiCarpool) {
				if (err){
					next(err);
				} else {
					next(null, _driver, taxiCarpool);
				}
			});
		}, function (_driver, _taxiCarpool, next) {
			if (!_taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			_taxiCarpool.registrant = req.params.registrant;
			_taxiCarpool.isValid = true;
			_taxiCarpool.isCatch = req.params.isCatch;
			_taxiCarpool.isPush = req.params.isPush;
			_taxiCarpool.money = parseInt(req.params.money);
			_taxiCarpool.start = {
				text: req.params.start_text,
				location: {
					type: "Point",
					coordinates: [parseFloat(req.params.start_lng), parseFloat(req.params.start_lat)]
				} 
			};
			_taxiCarpool.end = {
				text: req.params.stop_text,
				location: {
					type: "Point",
					coordinates: [parseFloat(req.params.stop_lng), parseFloat(req.params.stop_lat)]
				}
			};
			_taxiCarpool.drivers.max = parseInt(req.params.max);
			_taxiCarpool.time = parseInt(req.params.time);
			_taxiCarpool.divideN = (req.params.divide == 1);
			
			_taxiCarpool.save(function (err) {
				next(err, _taxiCarpool, _driver);
			});			
		}, function (taxiCarpool, _driver, next) {
			var driverInfo = _driver.getData();
			var taxiCarpoolInfo = taxiCarpool.getData();
			res.send({ result: main.result.SUCCESS, taxi: taxiCarpoolInfo });
			
			next(null, taxiCarpoolInfo, driverInfo);
		}, function(taxiCarpoolInfo, driverInfo, next){
			if(req.params.isPush == 1) {
				var lat = parseFloat(req.params.start_lat);
			    var lng = parseFloat(req.params.start_lng);
			    var limitTime = new Date().getTime() - (60 * 60 * 1000);
			    
			    TaxiFind.aggregate([
					{
						"$geoNear": {
							"near": {
								"type": "Point",
								"coordinates": [lng, lat]
							},
							"distanceField": "distance",
							"maxDistance": 5000,
							"spherical": true,
							"limit": 100000
						}
					},
					{
						"$match": {
							"date" : { "$gt" : limitTime }
						}
					}
				], function(err, finds) {
					next(err, taxiCarpoolInfo, driverInfo, finds);
				});				
			} else {
				next(null, taxiCarpoolInfo, driverInfo, null);
			}
		}, function(taxiCarpoolInfo, driverInfo, finds, next) {
			if(req.params.isPush == 1) {
				var nickName = req.params.phone.substr(req.params.phone.length - 4, 4);;
				var message = new gcm.Message({
	                delayWhileIdle: true,
	                timeToLive: 3,
	                data: {
	                    type: 'create_taxi',
	                    taxiId: taxiCarpoolInfo.taxiId,
	                    phone: req.params.phone,
	                    message: nickName + "기사님이 택시카풀신청을 수정하셨습니다."
	                }
	            });
				
				var retLength = finds.length;
				if(retLength > 0) {
					var retGcmId = [];
					for(var i = 0; i < retLength; i++) {
						if(finds[i].phone != req.params.phone) {
							retGcmId.push(finds[i].gcmId);							
						}
					}
					
					sender.send(message, retGcmId, function (err, result){
						if(err) {
							errFunction(err, req, res);
						}
//						logger.info("send gcm\n");
					});
				}
			}
			
			next(null, taxiCarpoolInfo, driverInfo);
		}, function(taxiCarpoolInfo, driverInfo, next) {
			var log_taxi = new _LogTaxi({
				driverId:	driverInfo.driverId,
				taxiId:		taxiCarpoolInfo.taxiId,
				isValid:	taxiCarpoolInfo.isValid,
				isPush:		taxiCarpoolInfo.isPush,
				isCatch:	taxiCarpoolInfo.isCatch,
				start: {
					text: taxiCarpoolInfo.start.text,
					location: {
						type: taxiCarpoolInfo.start.location.type,
						coordinates: taxiCarpoolInfo.start.location.coordinates
					}
				},
				end: {
					text: taxiCarpoolInfo.end.text,
					location: {
						type: taxiCarpoolInfo.end.location.type,
						coordinates: taxiCarpoolInfo.end.location.coordinates
					}
				},
				money:		taxiCarpoolInfo.money,
				host:		taxiCarpoolInfo.host,
				drivers: {
					max: taxiCarpoolInfo.drivers.max,
					list: taxiCarpoolInfo.drivers.list,
				},
				time:		taxiCarpoolInfo.time,
				divideN: 	taxiCarpoolInfo.divideN,
				req_driverId:	"",
				message:	"",
				logType:	"edit_share",
				logDate:	new Date()
			});
			
			log_taxi.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function acceptTaxi (req, res) {
	if (checkParams(['taxiId', 'phone', 'message'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function(taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			Driver.findOne({ phone: taxiCarpool.host }).exec(function (err, hostInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, next) {
			if (!hostInfo) {
				res.send({ result: main.result.NO_SUCH_TAXI_HOST });
				return;
			}
			
			Driver.findOne({ phone: req.params.phone }).exec(function (err, guestInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo, guestInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			if (!guestInfo) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			var driverIndex = -1;
			for (var i = 0; i < taxiCarpool.drivers.list.length; i++) {
				if (taxiCarpool.drivers.list[i].phone == guestInfo.phone) {
					driverIndex = i;
					break;
				}
			}
			
			if(driverIndex == -1) {
				res.send({ result: main.result.NO_SUCH_TAXI_REGISTER });
				return;
			} else if(taxiCarpool.drivers.list[driverIndex].status == "accept") {
				res.send({ result: main.result.DUPLICATE_ACCEPT });
				return;
			} else if(taxiCarpool.drivers.list[driverIndex].status == "decline") {
				res.send({ result: main.result.DUPLICATE_DECLINE });
				return;
			} else {
				taxiCarpool.drivers.list[driverIndex].isAccept = true;
				taxiCarpool.drivers.list[driverIndex].status = "accept";
				
				taxiCarpool.save(function (err) {
					next(err, taxiCarpool, hostInfo, guestInfo);
				});
			}
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			var message = new gcm.Message({
				delayWhileIdle: true,
				timeToLive: 3,
				data: {
					type: 'success_taxi',
					taxiId: req.params.taxiId,
					message: req.params.message
				}
			});
			
			sender.send(message, [guestInfo.gcmId], function (err, result) {
				next(err, taxiCarpool, hostInfo, guestInfo);
			});
	    }, function (_taxiCarpool, _hostInfo, _guestInfo, next) {
	    	var hostDriverInfo = _hostInfo.getData();
			var guestDriverInfo = _guestInfo.getData();
			var taxiCarpoolInfo = _taxiCarpool.getData();
			
			res.send({ result: main.result.SUCCESS });
			
			var log_taxi = new _LogTaxi({
				driverId:	hostDriverInfo.driverId,
				taxiId:		taxiCarpoolInfo.taxiId,
				isValid:	taxiCarpoolInfo.isValid,
				isPush:		taxiCarpoolInfo.isPush,
				isCatch:	taxiCarpoolInfo.isCatch,
				start: {
					text: taxiCarpoolInfo.start.text,
					location: {
						type: taxiCarpoolInfo.start.location.type,
						coordinates: taxiCarpoolInfo.start.location.coordinates
					}
				},
				end: {
					text: taxiCarpoolInfo.end.text,
					location: {
						type: taxiCarpoolInfo.end.location.type,
						coordinates: taxiCarpoolInfo.end.location.coordinates
					}
				},
				money:		taxiCarpoolInfo.money,
				host:		taxiCarpoolInfo.host,
				drivers: {
					max: taxiCarpoolInfo.drivers.max,
					list: taxiCarpoolInfo.drivers.list
				},
				time:		taxiCarpoolInfo.time,
				divideN: 	taxiCarpoolInfo.divideN,
				req_driverId:	guestDriverInfo.driverId,
				message:	req.params.message,
				logType:	"accept_share",
				logDate:	new Date()
			});
			
			log_taxi.save();
			
			next();
        }
    ], function (err) {
        if (err) errFunction(err, req, res);
    });
}

function declineTaxi (req, res) {
	if (checkParams(['taxiId', 'phone', 'message'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			TaxiCarpool.findById(new ObjectId(req.params.taxiId)).exec(next);
		}, function (taxiCarpool, next) {
			if (!taxiCarpool) {
				res.send({ result: main.result.NO_SUCH_TAXI });
				return;
			}
			
			Driver.findOne({ phone: taxiCarpool.host }).exec(function (err, hostInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, next) {
			if (!hostInfo) {
				res.send({ result: main.result.NO_SUCH_TAXI_HOST });
				return;
			}
			
			Driver.findOne({ phone: req.params.phone }).exec(function (err, guestInfo) {
				if(err) {
					next(err);
				} else {
					next(null, taxiCarpool, hostInfo, guestInfo);
				}
			});
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			if (!guestInfo) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			var driverIndex = -1;
			for (var i = 0; i < taxiCarpool.drivers.list.length; i++) {
				if (taxiCarpool.drivers.list[i].phone == guestInfo.phone) {
					driverIndex = i;
					break;
				}
			}
			
			if(driverIndex == -1) {
				res.send({ result: main.result.NO_SUCH_TAXI_REGISTER });
				return;
			} else if(taxiCarpool.drivers.list[driverIndex].status == "accept") {
				res.send({ result: main.result.DUPLICATE_ACCEPT });
				return;
			} else if(taxiCarpool.drivers.list[driverIndex].status == "decline") {
				res.send({ result: main.result.DUPLICATE_DECLINE });
				return;
			} else {
				taxiCarpool.drivers.list[driverIndex].status = "decline";
				
				taxiCarpool.save(function (err) {
					next(err, taxiCarpool, hostInfo, guestInfo);
				});
			}
		}, function (taxiCarpool, hostInfo, guestInfo, next) {
			var message = new gcm.Message({
				delayWhileIdle: true,
				timeToLive: 3,
				data: {
					type: 'decline_taxi',
					taxiId: req.params.taxiId,
					message: req.params.message
				}
			});
			
			sender.send(message, [guestInfo.gcmId], function (err, result) {
				next(err, taxiCarpool, hostInfo, guestInfo);
			});
		}, function (_taxiCarpool, _hostInfo, _guestInfo, next) {
			var hostDriverInfo = _hostInfo.getData();
			var guestDriverInfo = _guestInfo.getData();
			var taxiCarpoolInfo = _taxiCarpool.getData();
			
			res.send({ result: main.result.SUCCESS });
			
			var log_taxi = new _LogTaxi({
				driverId:	hostDriverInfo.driverId,
				taxiId:		taxiCarpoolInfo.taxiId,
				isValid:	taxiCarpoolInfo.isValid,
				isPush:		taxiCarpoolInfo.isPush,
				isCatch:	taxiCarpoolInfo.isCatch,
				start: {
					text: taxiCarpoolInfo.start.text,
					location: {
						type: taxiCarpoolInfo.start.location.type,
						coordinates: taxiCarpoolInfo.start.location.coordinates
					}
				},
				end: {
					text: taxiCarpoolInfo.end.text,
					location: {
						type: taxiCarpoolInfo.end.location.type,
						coordinates: taxiCarpoolInfo.end.location.coordinates
					}
				},
				money:		taxiCarpoolInfo.money,
				host:		taxiCarpoolInfo.host,
				drivers: {
					max: taxiCarpoolInfo.drivers.max,
					list: taxiCarpoolInfo.drivers.list
				},
				time:		taxiCarpoolInfo.time,
				divideN: 	taxiCarpoolInfo.divideN,
				req_driverId:	guestDriverInfo.driverId,
				message:	req.params.message,
				logType:	"decline_share",
				logDate:	new Date()
			});
			
			log_taxi.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function updateStatus (req, res) {
	var start_time = new Date().getTime();
	
	if (checkParams(['driverId', 'phone', 'lat', 'lng'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (_driver, next) {
			if (!_driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			TaxiFind.findOne({ driverId: req.params.driverId }).exec(function(err, find) {
				next(err, _driver, find);
			});
		}, function(_driver, _find, next) {
			var driverInfo = _driver.getData();
			var taxiFind = _find;
			if(!taxiFind) {
				taxiFind = new TaxiFind({
					driverId: req.params.driverId,
					phone: req.params.phone,
					date: new Date().getTime(),
					gcmId: driverInfo.gcmId,
					ynLogin: driverInfo.ynLogin,
					location: {
						type: "Point",
						coordinates: [ parseFloat(req.params.lng), parseFloat(req.params.lat)]
					} 
				});
			} else {
				taxiFind.ynLogin = driverInfo.ynLogin;
				taxiFind.date = new Date().getTime();
				taxiFind.location = {
					type: "Point",
					coordinates: [ parseFloat(req.params.lng), parseFloat(req.params.lat)]
				};
			}
			
			taxiFind.save(function(err) {
				next(err, taxiFind, driverInfo);
			});
		}, function(taxiFind, driverInfo, next) {
			res.send({ result: main.result.SUCCESS });
			
			var log_taxi_find = new _LogTaxiFind({
				driverId:	driverInfo.driverId,
				phone:		driverInfo.phone,
				gcmId: 		driverInfo.gcmId,
				ynLogin:	driverInfo.ynLogin,
				date:		taxiFind.date,
				location: {
					type: taxiFind.location.type,
					coordinates: taxiFind.location.coordinates
				},
				isEnable:	taxiFind.isEnable,
				logDate: new Date()
			});
			
			log_taxi_find.save();
			
			var end_time = new Date().getTime();
			logger.info((end_time - start_time) + "ms [" + req.getPath() + "]");
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function findStatus (req, res) {
	if (checkParams(['lat', 'lng', 'range'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			var lat = parseFloat(req.params.lat);
		    var lng = parseFloat(req.params.lng);
		    var range = parseFloat(req.params.range);
		    var limitTime = new Date().getTime() - (60 * 60 * 1000);
		    
		    TaxiFind.aggregate([
				{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [lng, lat]
						},
						"distanceField": "distance",
						"maxDistance": range,
						"spherical": true,
						"limit": 100000
					}
//				},
//				{
//				TODO : 앱 1.2.1 버전 업데이트 시 주석 제거
//					"$match": {
//                        "date" : { "$gt" : limitTime }
//						"ynLogin" : "Y"
//					}
				}
			], function(err, finds) {
				next(err, finds);
			});
		}, function (finds, next) {
			var retArray = [];
			for(var i = 0; i < finds.length; i++) {
				var retFind = {
					driverId: finds[i].driverId,
					phone:	finds[i].phone,
					date:	finds[i].date,
					location: {
						type: finds[i].location.type,
						coordinates: finds[i].location.coordinates
					}
				};
				
				retArray.push(retFind);
			}
			
			res.send({ result: main.result.SUCCESS, driver_list: retArray });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

exports.initialize = initialize;
exports.taxiList = taxiList;
exports.getTaxi = getTaxi;
exports.registerTaxi = registerTaxi;
exports.createTaxi = createTaxi;
exports.patchTaxi = patchTaxi;
exports.acceptTaxi = acceptTaxi;
exports.declineTaxi = declineTaxi;
exports.updateStatus = updateStatus;
exports.findStatus = findStatus;
exports.editTaxi = editTaxi;
exports.deleteTaxi = deleteTaxi;
exports.taxiRegistrantList = taxiRegistrantList;
