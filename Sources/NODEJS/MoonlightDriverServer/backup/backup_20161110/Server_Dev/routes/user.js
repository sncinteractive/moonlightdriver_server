var Async			= require('async');
var User      		= DB.model('User');
var UserVersion 	= DB.model('UserVersion');
var _LogUser		= DB.model('_LogUser');
var ObjectId		= DB.Types.ObjectId;

function initialize (server) {
	server.post('/user/register/auto', autoRegisterUser);
	server.post('/user/version', checkVersion);
	server.post('/user/update/location', updateUserLocation);
}

function checkVersion(req, res) {
	UserVersion.findOne({isValid: true}).exec(function (err, version) {
		if(err) {
			errFunction(err, req, res);
		} else {
			if(version) {
				var versionInfo = version.getData();
				if(versionInfo.version == req.params.version) {
					res.send({ result: main.result.SUCCESS });
				} else {
					res.send({ result: main.result.NEED_UPDATE_VERSION });
				}	
			} else {
				res.send({ result: main.result.NOT_ALLOW_VERSION });
			}
		}
	});
}

function autoRegisterUser(req, res) {
	if(checkParams(['phone', 'macAddr', 'gcmId', "device_uuid", "lat", "lng", "email", "name"], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var isRegister = true;
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
	    function(next) {
	    	User.findOne({ device_uuid: req.params.device_uuid }).exec(next);
	    }, function(_userInfo, next) {
	    	var user = null;
	    	if(!_userInfo) {
	    		isRegister = true;
	    		user = new User({
					gcmId: req.params.gcmId,
					email: req.params.email,
					phone: req.params.phone,
					device_uuid: req.params.device_uuid,
					name: req.params.name,
					macAddr: req.params.macAddr,
					profileImage: "",
					location: {
						type: "Point",
						coordinates: [ lng, lat ]
					},
					createdDate : new Date(),
					lastLoginDate : new Date()
				});
	    	} else {
	    		isRegister = false;
	    		user = _userInfo;
	    		if(req.params.gcmId) {
	    			user.gcmId = req.params.gcmId;
	    			user.macAddr = req.params.macAddr;
	    			user.phone = req.params.phone;
	    			user.lastLoginDate = new Date();
	    		}
	    	}
	    	
	    	user.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: main.result.SUCCESS, userDetail: user.getData() });
					next(null, user);
				}
			});
		}, function(_user, next) {
			var userInfo = _user.getData();
			var log_user = new _LogUser({
				userId: userInfo.userId,
				gcmId: userInfo.gcmId,
				device_uuid: userInfo.device_uuid,
				email:  userInfo.email,
				phone: userInfo.phone,
				name: userInfo.name,
				macAddr: userInfo.macAddr,
				profileImage: userInfo.profileImage,
				location: userInfo.location,
				logType: "",
				logDate: new Date()
			});
			
			if(isRegister) {
				log_user.logType = "reg_auto";
			} else {
				log_user.logType = "login";
			}
			
			log_user.save();
			next();
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

function updateUserLocation(req, res) {
	if(checkParams(['userId', 'lat', 'lng'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	var lat = parseFloat(req.params.lat);
	var lng = parseFloat(req.params.lng);
	
	Async.waterfall([
	    function(next) {
	    	User.findById(new ObjectId(req.params.userId)).exec(next);
	    }, function(_userInfo, next) {
	    	if(!_userInfo) {
				res.send({ result: main.result.NO_SUCH_USER });
				return;
	    	}
	    	
	    	_userInfo.location = {
	    		type: "Point",
				coordinates: [ lng, lat ]
	    	};
	    	
	    	_userInfo.save(function(err) {
				if(err) {
					next(err);
				} else {
					res.send({ result: main.result.SUCCESS });
					next();
				}
			});
		}
	], function(err) {
		if(err) {
			errFunction(err, req, res);
		}
	});
}

exports.initialize = initialize;
exports.autoRegisterUser = autoRegisterUser;
exports.checkVersion = checkVersion;
exports.updateUserLocation = updateUserLocation;
