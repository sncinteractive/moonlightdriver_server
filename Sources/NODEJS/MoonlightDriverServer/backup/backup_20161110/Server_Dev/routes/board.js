var Async		= require('async');
var Driver		= DB.model('Driver');
var Board		= DB.model('Board');
var _LogBoard	= DB.model('_LogBoard');
var _LogBoardComment = DB.model('_LogBoardComment');
var ObjectId	= DB.Types.ObjectId;
var gcm			= require("node-gcm");
var sender;

function initialize (server) {
	sender = new gcm.Sender("AIzaSyDb5DTLNl-uMS820Bozj5S1AVohg1g_uHY");
	server.post('/board/write/content', writeContent);
	server.post('/board/edit/content', editContent);
	server.post('/board/delete', deleteContent);
	server.post('/board/write/comment', writeComment);
    server.get('/board/list', getList);
    server.get('/board/:boardId/getone', getOneContent);
}

//Call 생성하는 API이다.
//API : HTTP POST /call
function writeContent (req, res) {
	var essentialParams = ['driverId', 'title', 'category', 'nickName', 'sex', 'age', 'career', 'divide', 'range', 'start', 'time', 'target', 'car', 'contact'];
	if (checkParams(essentialParams, req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			var board = new Board({
				driverId: req.params.driverId,
				nickName: req.params.nickName,
				title: req.params.title,
				category: req.params.category,
				sex: req.params.sex,
				age: req.params.age,
				career: req.params.career,
				divide: req.params.divide,
				range: req.params.range,
				start: req.params.start,
				time: req.params.time,
				target: req.params.target,
				car: req.params.car,
				contact: req.params.contact,
				etc: req.params.etc,
				comment: [],
				createdDate: new Date().getTime()
			});
			
			board.save(function (err) {
				next(err, board);
			});
		}, function (board, next) {
			var boardInfo = board.getData();
			res.send({ result: main.result.SUCCESS });
			
			var log_board = new _LogBoard({
				driverId: boardInfo.driverId,
				nickName: boardInfo.nickName,
				title: boardInfo.title,
				category: boardInfo.category,
				sex: boardInfo.sex,
				age: boardInfo.age,
				career: boardInfo.career,
				divide: boardInfo.divide,
				range: boardInfo.range,
				start: boardInfo.start,
				time: boardInfo.time,
				target: boardInfo.target,
				car: boardInfo.car,
				contact: boardInfo.contact,
				etc: boardInfo.etc,
				comment: boardInfo.comment,
				logStatus: "regist",
				logDate: new Date()
			});
			
			log_board.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function editContent (req, res) {
	var essentialParams = ['boardId', 'driverId', 'title', 'category', 'nickName', 'sex', 'age', 'career', 'divide', 'range', 'start', 'time', 'target', 'car', 'contact'];
	if (checkParams(essentialParams, req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function(driver, next) {
			if (!driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			Board.findById(new ObjectId(req.params.boardId)).exec(function(err, board) {
				next(err, driver, board);
			});
		}, function(driver, board, next) {
			if(!board) {
				res.send({ result: main.result.NO_SUCH_CONTENT });
				return;
			}
			
			if(board.driverId != req.params.driverId) {
				res.send({ result: main.result.NOT_MATCH_DRIVER });
				return;
			}
			
			board.nickName = req.params.nickName;
			board.title = req.params.title;
			board.category = req.params.category;
			board.sex = req.params.sex;
			board.age = req.params.age;
			board.career = req.params.career;
			board.divide = req.params.divide;
			board.range = req.params.range;
			board.start = req.params.start;
			board.time = req.params.time;
			board.target = req.params.target;
			board.car = req.params.car;
			board.contact = req.params.contact;
			board.etc = req.params.etc;
			
			board.save(function (err) {
				next(err, board);
			});
		}, function (board, next) {
			var boardInfo = board.getData();
			res.send({ result: main.result.SUCCESS });
			
			var log_board = new _LogBoard({
				driverId: boardInfo.driverId,
				nickName: boardInfo.nickName,
				title: boardInfo.title,
				category: boardInfo.category,
				sex: boardInfo.sex,
				age: boardInfo.age,
				career: boardInfo.career,
				divide: boardInfo.divide,
				range: boardInfo.range,
				start: boardInfo.start,
				time: boardInfo.time,
				target: boardInfo.target,
				car: boardInfo.car,
				contact: boardInfo.contact,
				etc: boardInfo.etc,
				comment: boardInfo.comment,
				logStatus: "edit",
				logDate: new Date()
			});
			
			log_board.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function deleteContent (req, res) {
	if (checkParams(['boardId', 'driverId'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function(driver, next) {
			if (!driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			Board.findById(new ObjectId(req.params.boardId)).exec(function(err, board) {
				next(err, driver, board);
			});
		}, function(driver, board, next) {
			if(!board) {
				res.send({ result: main.result.NO_SUCH_CONTENT });
				return;
			}
			
			if(board.driverId != req.params.driverId) {
				res.send({ result: main.result.NOT_MATCH_DRIVER });
				return;
			}
			
			Board.remove({"_id" : new ObjectId(req.params.boardId)}, function(err, data) {
				next(err, board);
			});
		}, function (board, next) {
			var boardInfo = board.getData();
			res.send({ result: main.result.SUCCESS });
			
			var log_board = new _LogBoard({
				driverId: boardInfo.driverId,
				nickName: boardInfo.nickName,
				title: boardInfo.title,
				category: boardInfo.category,
				sex: boardInfo.sex,
				age: boardInfo.age,
				career: boardInfo.career,
				divide: boardInfo.divide,
				range: boardInfo.range,
				start: boardInfo.start,
				time: boardInfo.time,
				target: boardInfo.target,
				car: boardInfo.car,
				contact: boardInfo.contact,
				etc: boardInfo.etc,
				comment: boardInfo.comment,
				logStatus: "delete",
				logDate: new Date()
			});
			
			log_board.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function writeComment (req, res) {
	if (checkParams(['driverId', 'boardId', 'nickName', 'comment'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Driver.findById(new ObjectId(req.params.driverId)).exec(next);
		}, function (driver, next) {
			if (!driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			Board.findById(new ObjectId(req.params.boardId)).exec(function(err, board) {
				next(err, driver, board);
			});
		}, function (driver, board, next) {
			if(!board) {
				res.send({ result: main.result.NO_SUCH_CONTENT });
				return;
			}
			
			for(var i = 0; i < board.comment.length; i++) {
				if(board.comment[i].driverId != req.params.driverId && board.comment[i].nickName == req.params.nickName) {
					res.send({ result: main.result.DUPLICATE_NICKNAME });
					return;
				}
			}
			
			var comment = {
				driverId: req.params.driverId,
				nickName: req.params.nickName,
				comment: req.params.comment,
				createdAt: new Date().getTime()
			};
			
			board.comment.push(comment);
			
			board.save(function(err) {
				if(err) {
					next(err);
				} else {
					next(null, board);
				}
			});
		}, function(board, next) {
			var boardInfo = board.getData();
			Driver.findById(new ObjectId(boardInfo.driverId)).exec(function(err, driver) {
//				logger.info(boardInfo);
				
				if(err) {
					next(err);
				} else {
					next(null, boardInfo, driver);
				}
			});
		}, function(board, driver, next) {
			if (!driver) {
				res.send({ result: main.result.NO_SUCH_DRIVER });
				return;
			}
			
			var driverInfo = driver.getData();
//			logger.info(driverInfo);
			var message = new gcm.Message({
				delayWhileIdle: true,
				timeToLive: 3,
				data: {
					type: 'twoinone_answer',
					boardId: board.boardId,
					message: "2인1조 등록글에 답글이 달렸습니다."
				}
			});
			
			sender.send(message, [driverInfo.gcmId], function (err, result) {
				next(err);
			});	
		}, function(next) {
			res.send({ result: main.result.SUCCESS });
			
			var log_comment = new _LogBoardComment({
				boardId : req.params.boardId,
				driverId: req.params.driverId,
				nickName: req.params.nickName,
				comment: req.params.comment,
				logStatus: "regist",
				logDate: new Date()
			});
			
			log_comment.save();
			
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getList (req, res) {
	Async.waterfall([
		function (next) {
			var pageNum = req.params.pageNum ? req.params.pageNum : 0;
			var limit = req.params.limit ? req.params.limit : 20;
			
			pageNum = pageNum * limit;
			
			Board.find().sort({ _id: -1 }).skip(pageNum).limit(limit).exec(next);
		}, function (boards, next) {
			var boardList = [];
			for (var i = 0; i < boards.length; i++) {
				boardList.push(boards[i].getListData());
			}
			
			res.send({ result: main.result.SUCCESS, list: boardList });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

function getOneContent (req, res) {
	if (checkParams(['boardId'], req.params)) {
		paramFunction(req, res);
		return;
	}
	
	Async.waterfall([
		function (next) {
			Board.findById(new ObjectId(req.params.boardId)).exec(next);
		}, function (content, next) {
			if(!content) {
				res.send({ result: main.result.NO_SUCH_CONTENT });
				return;
			}
			
			var contentInfo = content.getData();
			res.send({ result: main.result.SUCCESS, content: contentInfo });
			next();
		}
	], function (err) {
		if (err) errFunction(err, req, res);
	});
}

exports.initialize = initialize;
exports.writeContent = writeContent;
exports.editContent = editContent;
exports.deleteContent = deleteContent;
exports.writeComment = writeComment;
exports.getList = getList;
exports.getOneContent = getOneContent;
