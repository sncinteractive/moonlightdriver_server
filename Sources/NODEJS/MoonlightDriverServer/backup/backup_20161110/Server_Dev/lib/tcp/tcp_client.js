global.rootPath = process.argv[2];
global.config	= require(rootPath + '/inc/config');
var Logger		= require(rootPath + '/lib/Logger');
global.logger	= new Logger();

var connManager	= require(rootPath + '/lib/tcp/connection_manager');
var connStream	= require(rootPath + '/lib/tcp/connection_stream');

process.on('message', function(_recv_data) {
//	logger.info("message() recv_msg from master : " + JSON.stringify(_recv_data));
	
	switch(_recv_data.event_type) {
		case "connection" :
			connManager.connect(_recv_data.port, _recv_data.host, connStream);
			break;
		case "send_register_req" :
			connStream.sendRegisterPacket(_recv_data.unique_id, _recv_data.org_number);
			break;
		case "send_unregister_req" :
			connStream.sendUnregisterPacket(_recv_data.virtual_number);
			break;
	}
});

process.on("uncaughtException", function (err) {
	logger.error("tcp client process uncaughtException: " + err.stack);
});

String.prototype.startsWith = function( str ) {
	return this.substring(0, str.length) === str;
};

String.prototype.endsWith = function( str ) {
	return this.substring( this.length - str.length, this.length ) === str;
};
