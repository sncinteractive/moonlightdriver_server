var gcm	= require("node-gcm");

function PushMessage() {
	this.sender = new gcm.Sender(config.GCM.SENDER_ID);
	
	this.delayWhileIdle = true;
	this.timeToLive = 3;
};

PushMessage.prototype.createMessage = function (_message_data) {
	var message = new gcm.Message({
		delayWhileIdle: this.delayWhileIdle,
		timeToLive: this.timeToLive,
		data: _message_data
	});
	
	return message;
};

PushMessage.prototype.sendMessage = function (_message_data, _gcm_list, _callback) {
	var message = this.createMessage(_message_data);
		
	this.sender.send(message, _gcm_list, function (err, result) {
		_callback(err);
	});
};

module.exports = PushMessage;
