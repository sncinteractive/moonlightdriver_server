global.rootPath			= __dirname + "/..";
global.config			= require(rootPath + '/inc/config');
var DB					= require('../lib/Database');
var Logger				= require(rootPath + '/lib/Logger');
var SVRUtils			= require(rootPath + '/lib/SVRUtils');
var CardariDriver		= DB.model('CardariDriver');
var _LogUnderWriting	= DB.model('_LogUnderWriting');
var fs 					= require('fs');
var FtpClient			= require('ftp');

global.logger 			= new Logger(config.LOG.FILE.INFO, config.LOG.FILE.ERROR);
global.svr_utils		= new SVRUtils();

Date.prototype.getYYYYMMDD = function() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var date = now.getDate();
    
    month = month < 10 ? '0' + month : month;
    date = date < 10 ? '0' + date : date;
    
    return year + "" + month + "" + date;
}

var fileName = global.rootPath + "/insurance/driver_underwriting_" + new Date().getYYYYMMDD() + ".txt";
var separator = "BS";

CardariDriver.find({ insuranceStatus : "wait" }).exec(function(err, driverList) {
	console.log("===== TEST 1 =====");
	if(err) {
		console.log("===== TEST 2 =====");
		logger.error(err.stack);
	} else {
		console.log("===== TEST 3 =====");
		if(driverList && driverList.length > 0) {
			console.log("===== TEST 4 =====");
			var fileStream = fs.createWriteStream(fileName, {
				flags: 'a',
				encoding: 'utf8'
			});
			
			console.log("===== TEST 5 =====");
			for(var i = 0; i < driverList.length; i++) {
				console.log("===== TEST 6 =====");
				var driverInfo = driverList[i].getData();
				var ftpData = "";
				ftpData += config.INSURANCE.COMPANY_NO + separator;			// 사업자 번호
				ftpData += driverInfo.driverId + separator;			// 대리기사 관리 키
				ftpData += driverInfo.phone + separator;			// 대리기사 핸드폰번호
				ftpData += driverInfo.phone1 + separator;			// 대리기사 핸드폰번호2
				ftpData += driverInfo.name + separator;				// 대리기사 이름
				ftpData += driverInfo.socialNumber + separator;		// 대리기사 주민번호
				ftpData += driverInfo.licenseType + separator;		// 대리기사 운전면허종류
				ftpData += driverInfo.licenseNumber + separator;	// 대리기사 운전면허번호
				ftpData += driverInfo.email + separator;			// 대리기사 이메일
				ftpData += "" + separator;							// 기타 메모
				ftpData += "\n";
				
				fileStream.write(ftpData);
				
				(function(driverInfo) {
					console.log("===== TEST 7 =====");
					var log = new _LogUnderWriting({
						driverId: driverInfo.driverId,
						phone: driverInfo.phone,
						phone1: driverInfo.phone1,
						name: driverInfo.name,
						socialNumber: driverInfo.socialNumber,
						licenseType: driverInfo.licenseType,
						licenseNumber: driverInfo.licenseNumber,
						email: driverInfo.email,
						status:	"request",
						message: "",
						apiDriverId: driverInfo.apiDriverId,
						logType: "request"
					});
					
					log.save(function(_err) {
						if(_err) {
							logger.error(_err.stack);
						}
					});
				})(driverInfo);
			}
			
			console.log("===== TEST 8 =====");
			fileStream.end();
			console.log("===== TEST 9 =====");
			var client = new FtpClient();
			client.on('ready', function() {
				console.log("===== TEST 10 =====");
				client.put(fileName, fileName, function(err2) {
					console.log("===== TEST 11 =====");
					if (err2) {
						console.log("===== TEST 12 =====");
						logger.error("FTP UPLAOD FAIL");
						logger.error(err2.stack);
						
						process.exit(1);
					} else {
						console.log("===== TEST 13 =====");
						logger.info("FTP UPLAOD SUCCESS");
						
						CardariDriver.update({ insuranceStatus: "wait" }, { insuranceStatus: "process" }, { multi : true }).exec(function(_err) {
							console.log("===== TEST 14 =====");
							if(_err) {
								logger.error(_err.stack);
							}
							
							process.exit(1);
						});
					}
					console.log("===== TEST 15 =====");
					client.end();
				});
			});
			
			console.log("===== TEST 16 =====");
			client.on('error', function(_err) {
				console.log("===== TEST 17 =====");
				logger.error(_err);
			});
			
			console.log("===== TEST 18 =====");
			client.connect({
				host: "218.236.58.18",
				port: "22",
				user: "carpapp",
				password : "carapp_0101"
			});
		}
	}
});

process.on("uncaughtException", function (err) {
	logger.error("batch_req_underwriting process uncaughtException: " + err.stack);
});
