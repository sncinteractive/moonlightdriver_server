global.rootPath		= __dirname + "/..";
global.config		= require(rootPath + '/inc/config');
var DB				= require('../lib/Database');
var Logger			= require(rootPath + '/lib/Logger');
var SVRUtils		= require(rootPath + '/lib/SVRUtils');
var CardariDriver	= DB.model('CardariDriver');
var _LogUnderWriting	= DB.model('_LogUnderWriting');
var fs 				= require('fs');
var lineReader		= require('line-reader');

global.logger 		= new Logger(config.LOG.FILE.INFO, config.LOG.FILE.ERROR);
global.svr_utils	= new SVRUtils();

Date.prototype.getWeekOfYear = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

Date.prototype.getYYYYMMDD = function() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var date = now.getDate();
    
    month = month < 10 ? '0' + month : month;
    date = date < 10 ? '0' + date : date;
    
    return year + "" + month + "" + date;
}

Date.prototype.getYYYYMMDD_DASH = function() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var date = now.getDate();
    var hours = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    
    month = month < 10 ? '0' + month : month;
    date = date < 10 ? '0' + date : date;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;
    
    return year + "-" + month + "-" + date + " " + hours +":" + minutes + ":" + seconds;
}

var separator = "\b";
var downloadDirectory = global.rootPath + "/insurance/";
var fileName = "driver_underwriting_result_" + new Date().getYYYYMMDD() + ".txt";
//var fileName = "driver_underwriting_result_20181127.txt";

var Client = require('ssh2').Client;
var connSettings = {
	host: "218.236.58.18",
	port: "22",
	username: "carapp",
	password : "carapp_1001"
};

var conn = new Client();
conn.on('ready', function() {
	conn.sftp(function(err, sftp) {
        if (err) {
        	logger.error(errs);
        } else {
        	sftp.fastGet("/home/carapp/" + fileName, downloadDirectory + fileName, {}, function(downloadError){
                if(downloadError) {
                	logger.error(downloadError);
                	console.log(new Date().getYYYYMMDD_DASH() + " - [" + fileName + "] Failure Download");
                } else {
                	parseResponseData(downloadDirectory + fileName);
                    console.log(new Date().getYYYYMMDD_DASH() + " - [" + fileName + "] Succesfully Download");	
                }
            });	
        }
    });
}).connect(connSettings);

function parseResponseData(filePath) {
	lineReader.open(filePath, function(err, reader) {
		if (err) {
			logger.error(err);
			return;
		}
		
		if (reader.hasNextLine()) {
			reader.nextLine(function(err, data) {
				try {
					if (err) {
						logger.error(err);
					} else {
						updateUnderWritingResult(data);	
					}
				} finally {
					reader.close(function(err) {
						if (err) {
							logger.error(err);
						}
					});
				}
			});
		} else {
			reader.close(function(err) {
				if (err) {
					logger.error(err);
				}
			});
			
			setTimeout(function() {
				process.exit(1);
			}, 10 * 60 *1000);
		}
	});
}

function updateUnderWritingResult(data) {
	var dataArray = data.split(separator);
	var apiDriverId = dataArray[0];
	var uniqueId = dataArray[1];
	var socialNumber = dataArray[2];
	var result = dataArray[3];
	var insureNumber = dataArray[4];
	
	CardariDriver.findOne({uniqueId : uniqueId}).exec(function(_err, _driverInfo) {
		if(_err) {
			logger.error(_err.stack);
		} else {
			if(_driverInfo) {
				_driverInfo.apiDriverId = apiDriverId;
				_driverInfo.insuranceStatus = result;
				_driverInfo.insureNumber = insureNumber;
				
				if(result == "accepted" && (_driverInfo.driverStatus == "wait" || _driverInfo.driverStatus == "success")) {
					var availableDate = new Date();
					availableDate.setDate(availableDate.getDate() + 1);
					availableDate.setHours(0);
					availableDate.setMinutes(0);
					availableDate.setSeconds(0);
					
					_driverInfo.driverStatus = "success";
					_driverInfo.availableDate = availableDate;
				} else if(result != "accepted") {
					_driverInfo.driverStatus = "wait";
					_driverInfo.availableDate = null;
				}
				
				_driverInfo.save(function(_err) {
					if(_err) {
						logger.error(_err.stack);
					} else {
						var driverInfo = _driverInfo.getData();
						var log = new _LogUnderWriting({
							driverId: driverInfo.driverId,
							phone: driverInfo.phone,
							phone1: driverInfo.phone1,
							uniqueId: driverInfo.uniqueId,
							name: driverInfo.name,
							socialNumber: driverInfo.socialNumber,
							licenseType: driverInfo.licenseType,
							licenseNumber: driverInfo.licenseNumber,
							email: driverInfo.email,
							status:	result,
							apiDriverId: driverInfo.apiDriverId,
							message: getErrorMessage(result),
							logType: "response"
						});
						
						log.save(function(_err) {
							if(_err) {
								logger.error(_err.stack);
							}
						});
					}
				});
			}
		}
	});
}

function getErrorMessage(code) {
	if(code == "accepted") {
		return "통과";
	} else if(code == "rejected") {
		return "거절";
	} else if(code == "error1") {
		return "보험계약이 없는 경우입니다.";
	} else if(code == "error2") {
		return "기등록 기사입니다.";
	} else if(code == "error3") {
		return "사전조회 미동의";
	} else if(code == "error4") {
		return "계약체결미동의(보험사 에러)";
	} else if(code == "error5") {
		return "개발원조회오류";
	} else if(code == "error6") {
		return "보험 가입 불가 나이입니다.";
	} else if(code == "error7") {
		return "실명 오류입니다.";
	} else if(code == "error8") {
		return "주민번호 오류입니다.";
	} else if(code == "error9") {
		return "기타 (* 대리기사/탁송기사가 심사대기중일때, 같은기사(주민번호가 같은기사)를 보낼때)";
	} else if(code == "error10") {
		return "운행 금지 조치 기사 (도덕적 해이자/자기부담금 미납부자)";
	} else if(code == "error11") {
		return "주민등록번호가 없는경우(심사가 불가합니다.)";
	} else {
		return "정의되지 않은 오류 코드";
	}
}

process.on("uncaughtException", function (err) {
	logger.error("batch_req_underwriting process uncaughtException: " + err.stack);
});
