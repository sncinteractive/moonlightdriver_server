global.rootPath				= __dirname + "/..";
global.config				= require(rootPath + '/inc/config');
var schedule				= require('node-schedule');
var DB						= require('../lib/Database');
var Logger					= require(rootPath + '/lib/Logger');
var Crackdown				= DB.model('Crackdown');
var CrackdownRankWeekly		= DB.model('CrackdownRankWeekly');
var SVRUtils				= require(rootPath + '/lib/SVRUtils');

global.logger 		= new Logger(config.LOG.FILE.INFO, config.LOG.FILE.ERROR);
global.svr_utils	= new SVRUtils();

Date.prototype.getWeekOfYear = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

var now = new Date();
var startOfWeek = new Date(now.getFullYear(), now.getMonth(), (now.getDate() - now.getDay() + 1));
var endOfWeek = new Date(now.getFullYear(), now.getMonth(), now.getDate() + (7 - now.getDay()));

//console.log("startOfWeek", startOfWeek.getFullYear() + "-" + (startOfWeek.getMonth() + 1) + "-" + startOfWeek.getDate());
//console.log("endOfWeek", endOfWeek.getFullYear() + "-" + (endOfWeek.getMonth() + 1) + "-" + endOfWeek.getDate());

Crackdown.aggregate([
	{
		"$match" : {
			"from" : "카대리",
			"$and" : [
				{ "createTime" : {"$gte" : startOfWeek.getTime()} },
				{ "createTime" : {"$lt" : endOfWeek.getTime()} }
			]
		}
	},
	{
		"$group": {
			"_id" : { "userId" : "$userId", "registrant" : "$registrant", "phone" : "$phone" },
			"photoCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "photo" ] }, 1, 0
					]
				}
			},
			"normalCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "normal" ] }, 1, 0
					]
				}
			},
			"voiceCount" : { 
				"$sum": {
					"$cond" : [
						{ "$eq" : [ "$reportType", "voice" ] }, 1, 0
					]
				}
			},
			"point": {
				"$sum" : {
					"$cond" : [
						{ "$eq" : [ "$reportType", "photo" ] }, 2, 1
					]
				}
			}
		}
	}
], function(err, weeklyData) {
	if(err) {
		logger.error(err.stack);
	} else {
		var createTime = new Date().getTime();
		var startDateStr = startOfWeek.getFullYear() + "-" + (startOfWeek.getMonth() + 1  < 10 ? ('0' + (startOfWeek.getMonth() + 1)) : (startOfWeek.getMonth() + 1)) + "-" + (startOfWeek.getDate() < 10 ? ('0' + startOfWeek.getDate()) : startOfWeek.getDate());
		
		CrackdownRankWeekly.remove({"weekOfYear" : startOfWeek.getWeekOfYear()}).exec(function(_err, data) {
			if(_err) {
				logger.error(err.stack);
			} else {
				if(weeklyData && weeklyData.length > 0) {
					var listSize = weeklyData.length;
					for(var i = 0; i < listSize; i++) {
						var weeklyRankData = new CrackdownRankWeekly({
							weekStartDate:	startDateStr,
							weekOfYear:		startOfWeek.getWeekOfYear(),
							userId:			weeklyData[i]._id.userId,
							registrant: 	weeklyData[i]._id.registrant,
							phone: 			weeklyData[i]._id.phone,
							photoCount:		weeklyData[i].photoCount,
							normalCount: 	weeklyData[i].normalCount,
							voiceCount: 	weeklyData[i].voiceCount,
							totalCount:		weeklyData[i].photoCount + weeklyData[i].normalCount + weeklyData[i].voiceCount,
							point:			weeklyData[i].point,
							updateTime:		createTime,
							createTime: 	createTime
						});
						
						(function(weeklyRankData, i) {
							weeklyRankData.save(function(err) {
								if(err) {
									logger.error(err.stack);
								} else {
									logger.info(weeklyRankData.userId + " : " + weeklyRankData.totalCount + "(" + weeklyRankData.point + "point)");
								}
								
								if(i == listSize - 1) {
									process.exit(1);
								}
							});
						})(weeklyRankData, i);
					}
				} else {
					var loopCount = svr_utils.getRandomInt(3, 5);
					for(var i = 0; i < loopCount; i++) {
						var photoCount = svr_utils.getRandomInt(1, 20);
						var normalCount = svr_utils.getRandomInt(1, 20);
						var voiceCount = svr_utils.getRandomInt(1, 20);
						var totalPoint = (photoCount * 2) + (normalCount * 1) + (voiceCount * 1);
						
						var weeklyRankData = new CrackdownRankWeekly({
							weekStartDate:	startDateStr,
							weekOfYear:		startOfWeek.getWeekOfYear(),
							userId:			"",
							registrant: 	svr_utils.getRandPhoneNumber(),
							phone:			"",
							photoCount:		photoCount,
							normalCount: 	normalCount,
							voiceCount: 	voiceCount,
							totalCount:		photoCount + normalCount + voiceCount, 
							point:			totalPoint,
							updateTime:		createTime,
							createTime: 	createTime
						});
						
						(function(weeklyRankData, i) {
							weeklyRankData.save(function(err) {
								if(err) {
									logger.error(err.stack);
								} else {
									logger.info(weeklyRankData.userId + " : " + weeklyRankData.totalCount + "(" + weeklyRankData.point + "point)");
								}
								
								if(i == loopCount - 1) {
									process.exit(1);
								}
							});
						})(weeklyRankData, i);
					}
				}
			}
		});
	}
});

process.on("uncaughtException", function (err) {
	logger.error("batch process uncaughtException: " + err.stack);
});
